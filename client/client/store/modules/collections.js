import * as types from '../mutation-types'

const state = {
  data: {}
}

const mutations = {
  [types.GET_DATA_COLLECTIONS] (state, data) {
    state.collection = 'collection'
  }
}

export default {
  state,
  mutations
}
