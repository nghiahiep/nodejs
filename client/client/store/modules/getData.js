import Vue from 'vue'
import * as types from '../mutation-types'
import VueLocalStorage from 'vue-localstorage'
import vuemoment from 'vue-moment'
Vue.use(VueLocalStorage)
Vue.use(vuemoment)

const state = {
  users: Array,
  groups: Array,
  exts: Array,
  userDetail: {},
  stateAPI: {
    status: false,
    message: ''
  },
  groupView: {
    groupViewer: {},
    listGroup: []
  },
  inforCust: Object,
  inforPCB: Object,
  documents: Array,
  changeView: false,
  mafcTool: {
    deleteTSL: [],
    searchDRS: [],
    inforCust: [],
    inforCustNew: [],
    inforInsurance: []
  }
}

const mutations = {
  [types.GET_FILES_DOCUMENT] (state, data) {
    state.documents = data
  },
  [types.MAFC_DATA_PCB] (state, data) {
    state.inforPCB = data
  },
  [types.MAFC_DATA_SEARCH] (state, data) {
    state.inforCust = data
  },
  [types.ALL_INSURANCE] (state, data) {
    state.mafcTool.inforInsurance = data
  },
  [types.MAFC_DATA_SEARCH_TOOL] (state, data) {
    state.mafcTool = {
      deleteTSL: [data]
    }
  },
  [types.MAFC_DATA_DELETE_TOOL] (state, data) {
    state.mafcTool = {
      deleteTSL: []
    }
  },
  [types.MAFC_DATA_SEARCH_TEDRS_TOOL] (state, data) {
    state.mafcTool.searchDRS = [{
      INSPECTORID: data.rows[0][0],
      INSPECTORNAME: data.rows[0][1],
      REGION: data.rows[0][2],
      STATUS: data.rows[0][3],
      MAKEDATE: Vue.moment(data.rows[0][4]).format('DD-MM-YYYY HH:mm'),
      AUTHDATE: Vue.moment(data.rows[0][5]).format('DD-MM-YYYY HH:mm')
    }]
  },
  [types.MAFC_DATA_DELETE_TEDRS_TOOL] (state) {
    state.mafcTool.searchDRS = []
  },
  [types.MAFC_DATA_SEARCH_CUST_TOOL_1] (state, data) {
    state.mafcTool.inforCust = [{
      App_ID: data.appId,
      Inspector_Id: data.inspectorIdOld,
      Inspector_Name: data.inspectorNameOld
    }]
  },
  [types.MAFC_DATA_DELETE_CUST_TOOL_1] (state) {
    state.mafcTool.inforCust = []
  },
  [types.MAFC_DATA_SEARCH_CUST_TOOL_2] (state, data) {
    state.mafcTool.inforCustNew = [{
      Inspector_Id_NEW: data.inspectorIdNew,
      Inspector_Name_NEW: data.inspectorNameNew
    }]
  },
  [types.MAFC_DATA_DELETE_CUST_TOOL_2] (state) {
    state.mafcTool.inforCustNew = []
  },

  // group view
  [types.PUSH_GROUP_VIEWER] (state, data) {
    state.groupView = {
      groupViewer: data.groupViewer,
      listGroup: data.listGroup
    }
    Vue.localStorage.set('groupView',
      JSON.stringify({
        groupViewer: data.groupViewer,
        listGroup: data.listGroup
      })
    )
  },

  [types.API_PUT_USERDEFAULT_EXT] (state, data) {
    let userDefault = JSON.parse(Vue.localStorage.get('userDetail'))
    userDefault.ext = data
    Vue.localStorage.set('userDetail', JSON.stringify(userDefault))
  },

  [types.PUSH_GROUP_VIEWER_LOCAL] (state, data) {
    state.groupView = JSON.parse(Vue.localStorage.get('groupView'))
  },
  // infor user login local
  [types.PUT_INFOR_USER] (state, data) {
    state.userDetail = data
  },
  // users tab
  [types.API_GET_USERS] (state, data) {
    for (let i = 0; i < data.length; i++) {
      data[i].createdAt = Vue.moment(data[i].createdAt).format('DD-MM-YYYY HH:mm')
      data[i].updatedAt = Vue.moment(data[i].updatedAt).format('DD-MM-YYYY HH:mm')
    }
    state.users = data
  },

  // [types.API_GET_USER] (state, data) {
  //   state.userDetail = data
  // },

  // [types.API_POST_USER] (state, data) {
  //   state.userDetail = data
  // },

  [types.API_PUT_USER] (state, data) {
    data.createdAt = Vue.moment(data.createdAt).format('DD-MM-YYYY HH:mm')
    data.updatedAt = Vue.moment(data.updatedAt).format('DD-MM-YYYY HH:mm')
    let arr = state.users
    arr.find((item, index) => {
      if (item._id === data._id) {
        Vue.set(state.users, index, data)
      }
    })
    for (let i in state.groups) {
      if (state.groups[i]._id === data.groupOwner) {
        data.groupOwner = state.groups[i]
      }
    }
    state.users = arr
  },

  [types.PUSH_NEW_USER_STORE] (state, data) {
    data.createdAt = Vue.moment(data.createdAt).format('DD-MM-YYYY HH:mm')
    data.updatedAt = Vue.moment(data.updatedAt).format('DD-MM-YYYY HH:mm')
    state.users.unshift(data)
  },

  // [types.API_PUT_RESET_PASS] (state, data) {
  //   state.stateAPI = data
  // },

  [types.API_DELETE_USER] (state, data) {
    let arr = state.users
    let pos = arr.map(function (e) {
      return e._id
    }).indexOf(data)
    arr.splice(pos, 1)
    state.users = arr
  },
  // groups tab
  [types.API_GET_GROUPS] (state, data) {
    for (let i = 0; i < data.length; i++) {
      data[i].createdAt = Vue.moment(data[i].createdAt).format('DD-MM-YYYY HH:mm')
      data[i].updatedAt = Vue.moment(data[i].updatedAt).format('DD-MM-YYYY HH:mm')
    }
    state.groups = data
  },

  [types.API_PUT_GROUP] (state, data) {
    data.createdAt = Vue.moment(data.createdAt).format('DD-MM-YYYY HH:mm')
    data.updatedAt = Vue.moment(data.updatedAt).format('DD-MM-YYYY HH:mm')
    delete data.editor
    data.editor = JSON.parse(Vue.localStorage.get('userDetail'))
    let arr = state.groups
    for (let i in arr) {
      if (arr[i]._id === data.groupOwner) {
        delete data.groupOwner
        data.groupOwner = arr[i]
      } else {
        let grView = state.groupView.listGroup
        for (let i in grView) {
          if (data.groupOwner === grView[i]._id) {
            delete data.groupOwner
            data.groupOwner = {
              name: grView[i].name,
              _id: grView[i]._id
            }
          }
        }
      }
    }
    for (let i in arr) {
      let index = arr[i].groupChildrens.indexOf(data._id)
      if (index > -1) {
        arr[i].groupChildrens.splice(index, 1)
      }
      for (let j in arr[i].groupChildrens) {
        if (arr[i].groupChildrens[j] === data.groupOwner._id) {
          arr[i].groupChildrens.push(data._id)
        }
      }
      if (arr[i]._id === data.groupOwner._id) {
        arr[i].groupChildrens.push(data._id)
      }
    }
    arr.find((item, index) => {
      if (item._id === data._id) {
        Vue.set(state.groups, index, data)
      }
    })
  },

  [types.API_DELETE_GROUP] (state, data) {
    let arr = state.groups
    let pos = arr.map(function (e) {
      return e._id
    }).indexOf(data)
    arr.splice(pos, 1)
    for (let i in arr) {
      let index = arr[i].groupChildrens.indexOf(data)
      if (index > -1) {
        arr[i].groupChildrens.splice(index, 1)
      }
    }
    state.groups = arr
  },

  [types.PUSH_NEW_GROUP_STORE] (state, data) {
    data.data.createdAt = Vue.moment(data.data.createdAt).format('DD-MM-YYYY HH:mm')
    data.data.updatedAt = Vue.moment(data.data.updatedAt).format('DD-MM-YYYY HH:mm')
    data.data.editor = data.handle
    let arr = state.groups
    for (let i in arr) {
      for (let j in arr[i].groupChildrens) {
        if (arr[i].groupChildrens[j] === data.data.groupOwner._id) {
          arr[i].groupChildrens.push(data.data._id)
        }
      }
      if (data.data.groupOwner === arr[i]._id) {
        arr[i].groupChildrens.push(data.data._id)
        data.data.groupOwner = {
          name: arr[i].name,
          _id: arr[i]._id
        }
      }
    }
    let grView = state.groupView.listGroup
    for (let i in grView) {
      if (data.data.groupOwner === grView[i]._id) {
        data.data.groupOwner = {
          name: grView[i].name,
          _id: grView[i]._id
        }
      }
    }
    state.groups = arr
    state.groups.unshift(data.data)
  },
  // exts tab
  [types.API_GET_EXTS] (state, data) {
    for (let i = 0; i < data.length; i++) {
      data[i].createdAt = Vue.moment(data[i].createdAt).format('DD-MM-YYYY HH:mm')
      data[i].updatedAt = Vue.moment(data[i].updatedAt).format('DD-MM-YYYY HH:mm')
    }
    state.exts = data
  },

  [types.PUSH_NEW_EXT_STORE] (state, data) {
    data.createdAt = Vue.moment(data.createdAt).format('DD-MM-YYYY HH:mm')
    data.updatedAt = Vue.moment(data.updatedAt).format('DD-MM-YYYY HH:mm')
    state.exts.unshift(data)
  },

  [types.API_PUT_EXT] (state, data) {
    data.createdAt = Vue.moment(data.createdAt).format('DD-MM-YYYY HH:mm')
    data.updatedAt = Vue.moment(data.updatedAt).format('DD-MM-YYYY HH:mm')
    let arr = state.groups
    let user = state.users
    delete data.editor
    data.editor = JSON.parse(Vue.localStorage.get('userDetail'))
    for (let i in arr) {
      if (arr[i]._id === data.groupOwner) {
        delete data.groupOwner
        data.groupOwner = arr[i]
      }
    }
    for (let i in user) {
      if (user[i]._id === data.user) {
        delete data.user
        data.user = user[i]
      }
    }
    state.exts.find((item, index) => {
      if (item._id === data._id) {
        Vue.set(state.exts, index, data)
      }
    })
  },

  [types.API_DELETE_EXT] (state, data) {
    let arr = state.exts
    let pos = arr.map(function (e) {
      return e._id
    }).indexOf(data)
    arr.splice(pos, 1)
    state.exts = arr
  },

  // state api
  [types.RESET_STATE_API] (state, data) {
    state.stateAPI = {
      status: false,
      message: ''
    }
  },

  [types.STATE_API] (state, data) {
    state.stateAPI = data
  }
}

export default {
  state,
  mutations
}
