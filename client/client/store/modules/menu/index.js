import * as types from '../../mutation-types'
import lazyLoading from './lazyLoading'
// import charts from './charts'
// import uifeatures from './uifeatures'
// import components from './components'
// import tables from './tables'

// show: meta.label -> name
// name: component name
// meta.label: display label

const state = {
  items: [
    // {
    //   name: 'Collection',
    //   path: '/collection',
    //   meta: {
    //     icon: 'fa-usd',
    //     link: 'collection/index.vue'
    //   },
    //   component: lazyLoading('collection', true)
    // },
    // {
    //   name: 'ToolAD',
    //   path: '/mafcTool',
    //   meta: {
    //     auth: true,
    //     icon: 'fa-rocket',
    //     link: 'toolAD/index.vue'
    //   },
    //   component: lazyLoading('toolAD', true)
    // },
    // {
    //   name: 'Export Files',
    //   path: '/files',
    //   meta: {
    //     auth: true,
    //     icon: 'fa-file-excel-o',
    //     link: 'files/index.vue'
    //   },
    //   component: lazyLoading('files', true)
    // },
    {
      name: 'POS',
      path: '/pos',
      meta: {
        icon: 'fa-usd',
        link: 'pos/index.vue'
      },
      component: lazyLoading('pos', true)
    },
    {
      name: 'DashBoard POS',
      path: '/dashboardPOS',
      meta: {
        icon: 'fa-usd',
        link: 'dashboardPOS/index.vue'
      },
      component: lazyLoading('dashboardPOS', true)
    }
    // {
    //   name: 'Account',
    //   path: '/groups',
    //   meta: {
    //     icon: 'fa-user',
    //     link: 'groups/index.vue'
    //   },
    //   component: lazyLoading('groups', true)
    // }
    // {
    //   name: 'Dashboard',
    //   path: '/dashboard',
    //   meta: {
    //     icon: 'fa-tachometer',
    //     link: 'dashboard/index.vue'
    //   },
    //   component: lazyLoading('dashboard', true)
    // },
    // charts,
    // uifeatures,
    // components,
    // tables
  ]
}

const mutations = {
  [types.EXPAND_MENU] (state, menuItem) {
    if (menuItem.index > -1) {
      if (state.items[menuItem.index] && state.items[menuItem.index].meta) {
        state.items[menuItem.index].meta.expanded = menuItem.expanded
      }
    } else if (menuItem.item && 'expanded' in menuItem.item.meta) {
      menuItem.item.meta.expanded = menuItem.expanded
    }
  }
}

export default {
  state,
  mutations
}
