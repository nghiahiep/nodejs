import Vue from 'vue'
const state = {
  connect: Boolean,
  socketId: String,
  dataCollection: [],
  timeCount: 0
}

const mutations = {
  SOCKET_CONNECT: (state, status) => {
    state.connect = true
  },
  SOCKET_AUTH: (state, data) => {
    if (data[0].status) {
      state.socketId = data[0].room
    } else {
      state.socketId = null
    }
  },
  // SOCKET_BROADCAST: (state, data) => {
  //   let pushNew = true
  //   let newRooom = true
  //   state.timeCount = state.timeCount + 1
  //   for (let i = 0; i < state.dataCollection.length; i++) {
  //     if (state.dataCollection[i].group === data[0].group) {
  //       state.dataCollection[i].timeSystem = data[0].timeSystem
  //       newRooom = false
  //       for (let j in state.dataCollection[i].users) {
  //         let user = state.dataCollection[i].users[j]
  //         if (data[0].users[0]._id === user._id) {
  //           Vue.set(state.dataCollection[i].users, j, data[0].users[0])
  //           pushNew = false
  //         }
  //       }
  //       if (pushNew) {
  //         state.dataCollection[i].users.push(data[0].users[0])
  //       }
  //     }
  //   }

  //   if (newRooom) {
  //     state.dataCollection.push(data[0])
  //   }
  // },
  SOCKET_BROADCASTGETALL: (state, data) => {
    state.dataCollection = data[0]
    state.timeCount = state.timeCount + 1
  },
  SOCKET_UPDATEDEXT: (state, data) => {
    let pushNew = true
    let newRooom = true
    state.timeCount = state.timeCount + 1
    for (let i = 0; i < state.dataCollection.length; i++) {
      if (state.dataCollection[i].group === data[0].groupOwner.name) {
        state.dataCollection[i].timeSystem = data[0].timeUpdated
        newRooom = false
        for (let j in state.dataCollection[i].users) {
          let user = state.dataCollection[i].users[j]
          if (data[0].ext === user.ext) {
            Vue.set(state.dataCollection[i].users, j, data[0])
            pushNew = false
          }
        }
        if (pushNew) {
          state.dataCollection[i].users.push(data[0])
        }
      }
    }
    if (newRooom) {
      state.dataCollection.push({ group: data[0].groupOwner.name, timeSystem: data[0].timeUpdated, users: data[0] })
    }
  }
}

export default {
  state,
  mutations
}
