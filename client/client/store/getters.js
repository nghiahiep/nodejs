const pkg = state => state.pkg
const app = state => state.app
const device = state => state.app.device
const sidebar = state => state.app.sidebar
const effect = state => state.app.effect
const menuitems = state => state.menu.items
const dataSocket = state => state.socket
const getAllUsers = state => state.dataGetters.users
const getUserDetail = state => state.dataGetters.userDetail
const getStateApi = state => state.dataGetters.stateAPI
const getAllGroups = state => state.dataGetters.groups
const getAllExt = state => state.dataGetters.exts
const getGroupView = state => state.dataGetters.groupView
const getChangeViewStatus = state => state.dataGetters.changeView
const getInforUser = state => state.dataGetters.userDetail
const getMAFCTool = state => state.dataGetters.mafcTool
const getMAFCinforCust = state => state.dataGetters.inforCust
const getMAFCinforPCB = state => state.dataGetters.inforPCB
const getMAFCinforDocumenrs = state => state.dataGetters.documents
const componententry = state => {
  return state.menu.items.filter(c => c.meta && c.meta.label === 'Components')[0]
}

export {
  pkg,
  app,
  device,
  sidebar,
  effect,
  menuitems,
  getAllUsers,
  getUserDetail,
  getStateApi,
  getAllGroups,
  getGroupView,
  getChangeViewStatus,
  getInforUser,
  getAllExt,
  dataSocket,
  getMAFCTool,
  getMAFCinforCust,
  getMAFCinforPCB,
  getMAFCinforDocumenrs,
  componententry
}
