import * as types from './mutation-types'
import axios from 'axios'

export const toggleSidebar = ({ commit }, config) => {
  if (config instanceof Object) {
    commit(types.TOGGLE_SIDEBAR, config)
  }
}

export const toggleDevice = ({ commit }, device) => commit(types.TOGGLE_DEVICE, device)

export const expandMenu = ({ commit }, menuItem) => {
  if (menuItem) {
    menuItem.expanded = menuItem.expanded || false
    commit(types.EXPAND_MENU, menuItem)
  }
}

export const switchEffect = ({ commit }, effectItem) => {
  if (effectItem) {
    commit(types.SWITCH_EFFECT, effectItem)
  }
}

export const getDataCollections = ({ commit }, collections) => commit(types.GET_DATA_COLLECTIONS, collections)

export const hideMenu = ({ commit }, hideMenu) => commit(types.HIDE_MENU, hideMenu)

export const resetStateApi = ({ commit }) => commit(types.RESET_STATE_API)

export const setInforUser = ({ commit }, data) => commit(types.PUT_INFOR_USER, data)

export const callApiAllUser = ({ commit }) => {
  axios.get('user/')
  .then(response => {
    commit(types.API_GET_USERS, response.data.data)
  })
  .catch(e => {
    // this.errors.push(e)
  })
}

export const callApiDetailUser = ({ commit }, userId) => {
  axios.get('user/' + userId)
  .then(response => {
    commit(types.API_GET_USER, response.data.data)
  })
  .catch(e => {
  })
}

export const callApiCreateUser = ({ commit }, data) => {
  axios.post('user', data)
  .then(response => {
    if (response.data.status) {
      // commit(types.API_POST_USER, response.data.data)
      commit(types.PUSH_NEW_USER_STORE, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiEditUser = ({ commit }, data) => {
  axios.put('user/' + data.id, data.data)
  .then(response => {
    if (response.data.status) {
      commit(types.API_PUT_USER, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

// export const callApiChangePass = ({ commit }, data) => {
//   axios.put('user/' + data.id, data.data)
//   .then(response => {
//     if (response.data.status) {
//       commit(types.API_PUT_USER, response.data.data)
//     }
//     commit(types.STATE_API, { status: response.data.status, message: response.data.message })
//   })
//   .catch(e => {
//     commit(types.STATE_API, { status: false, message: e })
//   })
// }

export const callApiResetPass = ({ commit }, userId) => {
  axios.put('user/reset-password/' + userId)
  .then(response => {
    if (response.data.status) {
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiChangePass = ({ commit }, data) => {
  axios.put('user/change-password/' + data.id, data.data)
  .then(response => {
    if (response.data.status) {
      // commit(types.API_PUT_USER, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiDeleteUser = ({ commit }, userId) => {
  axios.delete('user/' + userId)
  .then(response => {
    if (response.data.status) {
      commit(types.API_DELETE_USER, userId)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiAllGroup = ({ commit }) => {
  axios.get('group')
  .then(response => {
    if (response.data.status) {
      commit(types.API_GET_GROUPS, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiCreateGroup = ({ commit }, data) => {
  axios.post('group', data.data)
  .then(response => {
    if (response.data.status) {
      commit(types.PUSH_NEW_GROUP_STORE, { data: response.data.data, handle: data.handle })
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiEditGroup = ({ commit }, data) => {
  axios.put('group/' + data.id, data.data)
  .then(response => {
    if (response.data.status) {
      commit(types.API_PUT_GROUP, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiDeleteGroup = ({ commit }, data) => {
  axios.delete('group/' + data)
  .then(response => {
    if (response.data.status) {
      commit(types.API_DELETE_GROUP, data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiAllExt = ({ commit }) => {
  axios.get('ext')
  .then(response => {
    if (response.data.status) {
      commit(types.API_GET_EXTS, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiCreateExt = ({ commit }, data) => {
  axios.post('ext', data)
  .then(response => {
    if (response.data.status) {
      commit(types.PUSH_NEW_EXT_STORE, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiSearchPOS = ({ commit }, data) => {
  axios.post('sql/searchQLDulieu', data)
  .then(response => {
    if (response.data.status) {
      commit(types.MAFC_DATA_SEARCH, response.data.data)
      commit(types.STATE_API, { status: null, message: null })
    } else {
      commit(types.STATE_API, { status: response.data.status, message: response.data.message })
      commit(types.MAFC_DATA_DELETE_TOOL)
    }
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiCheckPcb = ({ commit }, data) => {
  // let instance = axios.create({
  //   baseURL: 'http://10.20.0.80:8000/',
  //   timeout: 10000
  // })
  // instance.get('checkpcb/' + data)
  // .then(response => {
  //   commit(types.MAFC_DATA_PCB, response.data.result)
  //   commit(types.STATE_API, { status: null, message: null })
  // })
  // .catch(e => {
  //   commit(types.STATE_API, { status: false, message: e })
  // })
  axios.post('checkPCB', data)
  .then(response => {
    if (response.data.status) {
      commit(types.MAFC_DATA_PCB, JSON.parse(response.data.data).result)
      commit(types.STATE_API, { status: null, message: null })
    } else {
      commit(types.STATE_API, { status: response.data.status, message: response.data.message })
    }
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiReject = ({ commit }, data) => {
  axios.post('sql/rejectStage', data)
  .then(response => {
    if (response.data.status) {
      // commit(types.MAFC_DATA_SEARCH, response.data.data)
      commit(types.STATE_API, { status: null, message: null })
    } else {
      commit(types.STATE_API, { status: response.data.status, message: response.data.message })
      // commit(types.MAFC_DATA_DELETE_TOOL)
    }
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiNextStage = ({ commit }, data) => {
  axios.post('sql/nextStage', data)
  .then(response => {
    if (response.data.status) {
      // commit(types.MAFC_DATA_SEARCH, response.data.data)
      // this.$socket.emit('socketupdateStatus', { data: data, alert: response.data.status })
      commit(types.STATE_API, { status: null, message: null })
    } else {
      commit(types.STATE_API, { status: response.data.status, message: response.data.message })
      // commit(types.MAFC_DATA_DELETE_TOOL)
    }
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiUploadFile = ({ commit }, data) => {
  axios.post('upFiles', data)
  .then(response => {
    if (response.data.status) {
      // commit(types.MAFC_DATA_SEARCH, response.data.data)
      // this.$socket.emit('socketupdateStatus', { data: data, alert: response.data.status })
      commit(types.STATE_API, { status: null, message: null })
    } else {
      commit(types.STATE_API, { status: response.data.status, message: response.data.message })
      // commit(types.MAFC_DATA_DELETE_TOOL)
    }
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiGetDocument = ({ commit }, data) => {
  axios.post('getFiles', data)
  .then(response => {
    if (response.data.status) {
      commit(types.GET_FILES_DOCUMENT, response.data.data)
    } else {}
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const searchInsurance = ({ commit }, data) => {
  axios.post('sql/searchInsurance', data)
  .then(response => {
    if (response.data.status) {
      commit(types.ALL_INSURANCE, response.data.data)
    } else {}
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const excuteInsurance = ({ commit }, data) => {
  axios.post('sql/excuteInsurance', data)
  .then(response => {
    if (response.data.status) {
      commit(types.ALL_INSURANCE, response.data.data)
    } else {}
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

// export const callApiExecuteScript = ({ commit }, data) => {
//   axios.post('sql/executeScript', data)
//   .then(response => {
//     if (response.data.status) {
//       // commit(types.MAFC_DATA_DELETE_TOOL, response.data.data)
//     }
//     commit(types.STATE_API, { status: response.data.status, message: response.data.message })
//   })
//   .catch(e => {
//     commit(types.STATE_API, { status: false, message: e })
//   })
// }

export const callApiExtlUser = ({ commit }) => {
  axios.get('ext-user')
  .then(response => {
    if (response.data.status) {
      commit(types.API_PUT_USERDEFAULT_EXT, response.data.data)
    } else {
      commit(types.API_PUT_USERDEFAULT_EXT, null)
    }
  })
  .catch(e => {
  })
}

export const callApiUpdateExtlUser = ({ commit }, data) => {
  axios.put('ext-user/' + data.data, { user: data.id })
  .then(response => {
    if (response.data.status) {
      commit(types.API_PUT_EXT, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiUpdateExtStatus = ({ commit }, data) => {
  axios.put('ext-status/' + data.id, data.data)
  .then(response => {
    if (response.data.status) {
      commit(types.API_PUT_EXT, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiUpdateExt = ({ commit }, data) => {
  axios.put('ext/' + data.id, data.data)
  .then(response => {
    if (response.data.status) {
      commit(types.API_PUT_EXT, response.data.data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiDeleteExt = ({ commit }, data) => {
  axios.delete('ext/' + data)
  .then(response => {
    if (response.data.status) {
      commit(types.API_DELETE_EXT, data)
    }
    commit(types.STATE_API, { status: response.data.status, message: response.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}

export const callApiGetGroupSelect = ({ commit }, data) => {
  if (data.edit) {
    axios.get('group-select').then(response => {
      if (response.data.status) {
        let listGr = response.data.data
        let grView = {}
        for (let i = 0; i < listGr.length; i++) {
          if (data.groupViewer === listGr[i]._id) {
            grView = listGr[i]
          }
        }
        commit(types.PUSH_GROUP_VIEWER, { groupViewer: grView, listGroup: listGr })
      }
      commit(types.STATE_API, { status: response.data.status, message: response.data.message })
    })
    .catch(e => {
      commit(types.STATE_API, { status: false, message: e })
    })
  } else {
    commit(types.PUSH_GROUP_VIEWER_LOCAL, false)
  }
}

export const callApiChangeGroupDefault = ({ commit }, data) => {
  axios.put('user/change-group/' + data.userId, { groupDefault: data.groupId }).then(res => {
    if (res.data.status) {
      axios.get('group-select').then(response => {
        if (response.data.status) {
          let listGr = response.data.data
          let grView = {}
          for (let i = 0; i < listGr.length; i++) {
            if (data.groupId === listGr[i]._id) {
              grView = listGr[i]
            }
          }
          commit(types.PUSH_GROUP_VIEWER, { edit: true, groupViewer: grView, listGroup: listGr })
        }
        commit(types.STATE_API, { status: response.data.status, message: response.data.message })
      })
      .catch(e => {
        commit(types.STATE_API, { status: false, message: e })
      })
    }
    commit(types.STATE_API, { status: res.data.status, message: res.data.message })
  })
  .catch(e => {
    commit(types.STATE_API, { status: false, message: e })
  })
}
