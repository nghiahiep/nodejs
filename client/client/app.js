import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueAuth from '@websanova/vue-auth'
import NProgress from 'vue-nprogress'
import { sync } from 'vuex-router-sync'
import App from './App.vue'
import router from './router'
import store from './store'
import * as filters from './filters'
import { TOGGLE_SIDEBAR } from 'vuex-store/mutation-types'
import vuemoment from 'vue-moment'
// import VueSocketio from 'vue-socket.io'
// import io from 'socket.io-client'
import VueGoodTable from 'vue-good-table'
import 'vue-good-table/dist/vue-good-table.css'
import VueLocalStorage from 'vue-localstorage'
import io from 'socket.io-client'
import VueSocketio from 'vue-socket.io'
// import socketStore from 'socketStore

Vue.router = router
Vue.use(vuemoment)
Vue.use(VueGoodTable)
Vue.use(VueAxios, axios)
Vue.use(VueLocalStorage)
Vue.use(VueLocalStorage, {
  name: 'local',
  bind: true
})

// const serverUrl = 'http://192.168.100.50:3000'
// const serverUrl = 'http://172.17.5.80:3000'
const serverUrl = 'http://192.168.100.51:3030'
// const serverUrl = 'http://localhost:3000'
Vue.axios.defaults.baseURL = serverUrl + '/v1/'
// const socketInstance = io(serverUrl + '/socket', { transports: ['websocket'] }, store)
// Vue.use(VueSocketio, socketInstance)
let tokenRefresh = true
Vue.use(VueAuth, {
  auth: {
    request: function (req, token) {
      this.options.http._setHeaders.call(this, req, {token: token})
    },
    response: function (res) {
      if (tokenRefresh) {
        tokenRefresh = false
        const socketInstance = io(serverUrl + '/', {
          transports: ['websocket'],
          query: `token=${(res.data.data || {}).token}`
        })
        Vue.use(VueSocketio, socketInstance, store)
        return (res.data.data || {}).token
      }
    }
  },
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  loginData: { fetchUser: true },
  refreshData: { enabled: false },
  parseUserData: function (data) { return data.data },
  tokenDefaultName: 'token'
})

Vue.use(NProgress)

// Enable devtools
Vue.config.devtools = true

sync(store, router)

const nprogress = new NProgress({ parent: '.nprogress-container' })

const { state } = store

router.beforeEach((route, redirect, next) => {
  if (state.app.device.isMobile && state.app.sidebar.opened) {
    store.commit(TOGGLE_SIDEBAR, false)
  }
  next()
})

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

const app = new Vue({
  router,
  store,
  nprogress,
  ...App
})

export { app, router, store }
