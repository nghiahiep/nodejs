__workerId = __cluster.worker.id
__express = require('express')
__app = __express()
__server = require('http').createServer(__app).listen(process.env.PORT)
__app.use(__express.static(__dirname + '/dist/'))
__app.use((req, res, next) => {
    if (req.url === '/login' && req.method === 'GET') {
        res.sendFile(__dirname + '/dist/index.html')
    } else {
        next()
    }
})
__app.listen(8078)

if (__workerId == 1) {
    console.log(`Number cluster ${process.env.CPU_NUMBER}`)
}

require('./helpers')
require('./models')
require('./controllers')
require('./routers')
