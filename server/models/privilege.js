

const privilege = __mongoose.model('privilege', new __schema({
    ...__base,
    _id: { type: String, required: true }
}, { strict: false }))

exports.count = (query) => {
    return privilege.count(query)
}

exports.create = (object) => {
    return privilege.create(object)
}

exports.reads = (query, skip, limit, sort, select) => {
    return privilege.find(query, select, { skip, limit, sort })
}

exports.read = (query, select) => {
    return privilege.findOne(query, select)
}

exports.update = (query, object) => {
    return privilege.findOneAndUpdate(query, object)
}

exports.delete = (query) => {
    return privilege.findOneAndRemove(query)
}