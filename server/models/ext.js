
const ext = __mongoose.model('ext', new __schema({
    ...__base,
    ext: { type: String, required: true },
    password: { type: String, required: true },
    host:{ type: String, required: true },
    user: { type: String, ref: 'user' }
}, { strict: false }))

exports.count = (query) => {
    return ext.count(query)
}

exports.create = (object) => {
    return ext.create(object)
}

exports.reads = (query, skip, limit, sort, select) => {
    return ext.find(query, select, { skip, limit, sort })
}

exports.read = (query, select) => {
    return ext.findOne(query, select)
}

exports.update = (query, object) => {
    return ext.findOneAndUpdate(query, object)
}

exports.delete = (query) => {
    return ext.findOneAndRemove(query)
}

exports.updateMulti = (query, object) => {
    return ext.update(query, object, { multi: true })
}

exports.distinct = (query, field) => {
    return ext.distinct(field, query)
}