
const user = __mongoose.model('user', new __schema({
    ...__base,
    user: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    salt: { type: String, required: true },
    type: { type: String, required: true },
    token: { type: String, required: true },
    firstName: { type: String },
    lastName: { type: String },
    phone: { type: String },
    gender: { type: String },
    birthDay: { type: Date },
    urlImage: { type: String },
    typeSocial: { type: String },
    role: { type: String, ref: 'role' },
    roleFiles : { type: Array }
}, { strict: false }))

exports.count = (query) => {
    return user.count(query)
}

exports.create = (object) => {
    return user.create(object)
}

exports.reads = (query, skip, limit, sort, select) => {
    return user.find(query, select, { skip, limit, sort })
}

exports.read = (query, select) => {
    return user.findOne(query, select)
}

exports.update = (query, object) => {
    return user.findOneAndUpdate(query, object)
}

exports.delete = (query) => {
    return user.findOneAndRemove(query)
}