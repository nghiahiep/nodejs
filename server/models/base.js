module.exports = {
    groupOwner: { type: String, required: true, ref: 'group' },
    creator: { type: String, ref: 'user' },
    editor: { type: String, ref: 'user' },
    createdAt: Date,
    updatedAt: Date
}