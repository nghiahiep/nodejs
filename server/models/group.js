
const group = __mongoose.model('group', new __schema({
    ...__base,
    name: { type: String, required: true },
    groupChildrens: [{ type: String, ref: 'group' }]
}, { strict: false }))

exports.count = (query) => {
    return group.count(query)
}

exports.create = (object) => {
    return group.create(object)
}

exports.reads = (query, skip, limit, sort, select) => {
    return group.find(query, select, { skip, limit, sort })
}

exports.read = (query, select) => {
    return group.findOne(query, select)
}

exports.update = (query, object) => {
    return group.findOneAndUpdate(query, object)
}

exports.delete = (query) => {
    return group.findOneAndRemove(query)
}

exports.updateMulti = (query, object) => {
    return group.update(query, object, { multi: true })
}

exports.distinct = (query, field) => {
    return group.distinct(field, query)
}