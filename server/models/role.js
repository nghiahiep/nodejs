

const role = __mongoose.model('role', new __schema({
    ...__base,
    name: { type: String, required: true },
    privileges: [{ type: String, ref: 'privilege' }]
}, { strict: false }))

exports.count = (query) => {
    return role.count(query)
}

exports.create = (object) => {
    return role.create(object)
}

exports.reads = (query, skip, limit, sort, select) => {
    return role.find(query, select, { skip, limit, sort })
}

exports.read = (query, select) => {
    return role.findOne(query, select)
}

exports.update = (query, object) => {
    return role.findOneAndUpdate(query, object)
}

exports.delete = (query) => {
    return role.findOneAndRemove(query)
}