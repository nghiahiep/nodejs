
db.users.insert({
    "_id": ObjectId("5a7d48755b225753411f6db5"),
    "firstName": "Administration",
    "lastName": "",
    "email": "admin@vnwalls.com",
    "salt": "EVzqxlt5aJlnEs5RZfMrNZR22iDRo2S0",
    "password": "sha1$79ba3932$1$42096a0f9bec66f4095bfa223892aae963efb5c0",
    "token": "4WprOJOLWH8uSATZlLSgj3IdWA1FvqMg",
    "type": "ADMIN",
    "groupOwner": "5ab8608b6fe8d80764137de7",
    "creator": "5a7d48755b225753411f6db5",
    "editor": "5a7d48755b225753411f6db5",
    "createdAt": ISODate("2018-02-09T14:06:29.691+07:00"),
    "updatedAt": ISODate("2018-02-09T14:06:29.691+07:00"),
    "__v": 0
})

db.groups.insert([
    {
        "_id": ObjectId("5ab8608b6fe8d80764137de7"),
        "name": "Administration",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-03-26T09:52:59.258+07:00"),
        "updatedAt": ISODate("2018-03-26T09:52:59.258+07:00"),
        "groupChildrens": [
            "5acf22069e33652690881dfc",
            "5acf22329e33652690881dfd"
        ],
        "__v": 0
    },
    {
        "_id": ObjectId("5acf22069e33652690881dfc"),
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "name": "TESTING",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-12T16:08:22.025+07:00"),
        "updatedAt": ISODate("2018-04-12T16:08:22.025+07:00"),
        "groupChildrens": [],
        "__v": 0
    },
    {
        "_id": ObjectId("5acf22329e33652690881dfd"),
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "name": "VIET NAM",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-12T16:09:06.381+07:00"),
        "updatedAt": ISODate("2018-04-12T16:09:06.381+07:00"),
        "groupChildrens": [],
        "__v": 0
    }
])

db.privileges.insert([
    {
        "_id": "MODULE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:52:49.868+07:00"),
        "updatedAt": ISODate("2018-04-16T16:52:49.868+07:00"),
        "__v": 0
    },
    {
        "_id": "MODULE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:53:18.045+07:00"),
        "updatedAt": ISODate("2018-04-16T16:53:18.045+07:00"),
        "__v": 0
    },
    {
        "_id": "MODULE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:53:23.749+07:00"),
        "updatedAt": ISODate("2018-04-16T16:53:23.749+07:00"),
        "__v": 0
    },
    {
        "_id": "ROLE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:53:41.270+07:00"),
        "updatedAt": ISODate("2018-04-16T16:53:41.270+07:00"),
        "__v": 0
    },
    {
        "_id": "ROLE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:53:45.497+07:00"),
        "updatedAt": ISODate("2018-04-16T16:53:45.497+07:00"),
        "__v": 0
    },
    {
        "_id": "ROLE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:53:49.796+07:00"),
        "updatedAt": ISODate("2018-04-16T16:53:49.796+07:00"),
        "__v": 0
    },
    {
        "_id": "GROUP_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:54:59.950+07:00"),
        "updatedAt": ISODate("2018-04-16T16:54:59.950+07:00"),
        "__v": 0
    },
    {
        "_id": "GROUP_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:05.866+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:05.866+07:00"),
        "__v": 0
    },
    {
        "_id": "GROUP_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:10.924+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:10.924+07:00"),
        "__v": 0
    },
    {
        "_id": "USER_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:18.277+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:18.277+07:00"),
        "__v": 0
    },
    {
        "_id": "USER_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:22.264+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:22.264+07:00"),
        "__v": 0
    },
    {
        "_id": "USER_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:26.226+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:26.226+07:00"),
        "__v": 0
    },
    {
        "_id": "BANK_CARD_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:45.334+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:45.334+07:00"),
        "__v": 0
    },
    {
        "_id": "BANK_CARD_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:49.516+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:49.516+07:00"),
        "__v": 0
    },
    {
        "_id": "BANK_CARD_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:52.931+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:52.931+07:00"),
        "__v": 0
    },
    {
        "_id": "BANK_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:55:58.819+07:00"),
        "updatedAt": ISODate("2018-04-16T16:55:58.819+07:00"),
        "__v": 0
    },
    {
        "_id": "BANK_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:02.576+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:02.576+07:00"),
        "__v": 0
    },
    {
        "_id": "BANK_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:06.661+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:06.661+07:00"),
        "__v": 0
    },
    {
        "_id": "CATEGORY_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:18.953+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:18.953+07:00"),
        "__v": 0
    },
    {
        "_id": "CATEGORY_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:23.633+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:23.633+07:00"),
        "__v": 0
    },
    {
        "_id": "CATEGORY_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:28.513+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:28.513+07:00"),
        "__v": 0
    },
    {
        "_id": "PRODUCT_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:37.173+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:37.173+07:00"),
        "__v": 0
    },
    {
        "_id": "PRODUCT_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:41.409+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:41.409+07:00"),
        "__v": 0
    },
    {
        "_id": "PRODUCT_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:45.179+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:45.179+07:00"),
        "__v": 0
    },
    {
        "_id": "ORDER_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:52.514+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:52.514+07:00"),
        "__v": 0
    },
    {
        "_id": "ORDER_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:56:57.834+07:00"),
        "updatedAt": ISODate("2018-04-16T16:56:57.834+07:00"),
        "__v": 0
    },
    {
        "_id": "ORDER_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:01.924+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:01.924+07:00"),
        "__v": 0
    },
    {
        "_id": "QUESTION_ANSWER_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:14.122+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:14.122+07:00"),
        "__v": 0
    },
    {
        "_id": "QUESTION_ANSWER_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:18.325+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:18.325+07:00"),
        "__v": 0
    },
    {
        "_id": "QUESTION_ANSWER_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:22.057+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:22.057+07:00"),
        "__v": 0
    },
    {
        "_id": "PREFERENCE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:31.816+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:31.816+07:00"),
        "__v": 0
    },
    {
        "_id": "PREFERENCE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:35.764+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:35.764+07:00"),
        "__v": 0
    },
    {
        "_id": "PREFERENCE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:39.706+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:39.706+07:00"),
        "__v": 0
    },
    {
        "_id": "GUIDE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:45.871+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:45.871+07:00"),
        "__v": 0
    },
    {
        "_id": "GUIDE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:50.043+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:50.043+07:00"),
        "__v": 0
    },
    {
        "_id": "GUIDE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:53.713+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:53.713+07:00"),
        "__v": 0
    },
    {
        "_id": "CONTACT_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:57:58.559+07:00"),
        "updatedAt": ISODate("2018-04-16T16:57:58.559+07:00"),
        "__v": 0
    },
    {
        "_id": "CONTACT_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:58:02.723+07:00"),
        "updatedAt": ISODate("2018-04-16T16:58:02.723+07:00"),
        "__v": 0
    },
    {
        "_id": "CONTACT_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:58:06.522+07:00"),
        "updatedAt": ISODate("2018-04-16T16:58:06.522+07:00"),
        "__v": 0
    },
    {
        "_id": "FEEDBACK_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:58:13.718+07:00"),
        "updatedAt": ISODate("2018-04-16T16:58:13.718+07:00"),
        "__v": 0
    },
    {
        "_id": "FEEDBACK_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:58:17.139+07:00"),
        "updatedAt": ISODate("2018-04-16T16:58:17.139+07:00"),
        "__v": 0
    },
    {
        "_id": "FEEDBACK_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:58:21.019+07:00"),
        "updatedAt": ISODate("2018-04-16T16:58:21.019+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:58:57.566+07:00"),
        "updatedAt": ISODate("2018-04-16T16:58:57.566+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:00.941+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:00.941+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:04.653+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:04.653+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_VERSION_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:14.572+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:14.572+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_VERSION_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:18.426+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:18.426+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_VERSION_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:22.869+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:22.869+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_ZONE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:37.544+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:37.544+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_ZONE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:41.425+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:41.425+07:00"),
        "__v": 0
    },
    {
        "_id": "MACHINE_ZONE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:45.289+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:45.289+07:00"),
        "__v": 0
    },
    {
        "_id": "FIRMWARE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:54.332+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:54.332+07:00"),
        "__v": 0
    },
    {
        "_id": "FIRMWARE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T16:59:58.156+07:00"),
        "updatedAt": ISODate("2018-04-16T16:59:58.156+07:00"),
        "__v": 0
    },
    {
        "_id": "FIRMWARE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:02.066+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:02.066+07:00"),
        "__v": 0
    },
    {
        "_id": "FIRMWARE_EXECUTE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:09.955+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:09.955+07:00"),
        "__v": 0
    },
    {
        "_id": "FIRMWARE_EXECUTE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:13.940+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:13.940+07:00"),
        "__v": 0
    },
    {
        "_id": "FIRMWARE_EXECUTE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:17.921+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:17.921+07:00"),
        "__v": 0
    },
    {
        "_id": "REGIME_PLANT_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:23.737+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:23.737+07:00"),
        "__v": 0
    },
    {
        "_id": "REGIME_PLANT_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:28.377+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:28.377+07:00"),
        "__v": 0
    },
    {
        "_id": "REGIME_PLANT_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:32.614+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:32.614+07:00"),
        "__v": 0
    },
    {
        "_id": "REGIME_PLANT_EXECUTE_DELETE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:42.675+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:42.675+07:00"),
        "__v": 0
    },
    {
        "_id": "REGIME_PLANT_EXECUTE_WRITE",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:48.636+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:48.636+07:00"),
        "__v": 0
    },
    {
        "_id": "REGIME_PLANT_EXECUTE_READ",
        "groupOwner": "5ab8608b6fe8d80764137de7",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:00:52.420+07:00"),
        "updatedAt": ISODate("2018-04-16T17:00:52.420+07:00"),
        "__v": 0
    }
])

db.modules.insert([
    {
        "_id": "001",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Led Panel",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:31:53.849+07:00"),
        "updatedAt": ISODate("2018-04-16T17:31:53.849+07:00"),
        "__v": 0
    },
    {
        "_id": "002",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Ventilator",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:38:12.819+07:00"),
        "updatedAt": ISODate("2018-04-16T17:38:12.819+07:00"),
        "__v": 0
    },
    {
        "_id": "003",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Flow Sensor",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:38:22.974+07:00"),
        "updatedAt": ISODate("2018-04-16T17:38:22.974+07:00"),
        "__v": 0
    },
    {
        "_id": "004",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Solenoid Valve",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:38:31.696+07:00"),
        "updatedAt": ISODate("2018-04-16T17:38:31.696+07:00"),
        "__v": 0
    },
    {
        "_id": "005",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Pump",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:38:41.693+07:00"),
        "updatedAt": ISODate("2018-04-16T17:38:41.693+07:00"),
        "__v": 0
    },
    {
        "_id": "006",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Water Sensor",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:38:51.351+07:00"),
        "updatedAt": ISODate("2018-04-16T17:38:51.351+07:00"),
        "__v": 0
    },
    {
        "_id": "007",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Power Supply",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:39:00.658+07:00"),
        "updatedAt": ISODate("2018-04-16T17:39:00.658+07:00"),
        "__v": 0
    },
    {
        "_id": "008",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "General",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:39:11.855+07:00"),
        "updatedAt": ISODate("2018-04-16T17:39:11.855+07:00"),
        "__v": 0
    },
    {
        "_id": "009",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Hudimity Sensor",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:39:20.861+07:00"),
        "updatedAt": ISODate("2018-04-16T17:39:20.861+07:00"),
        "__v": 0
    },
    {
        "_id": "010",
        "groupOwner": "5acf22329e33652690881dfd",
        "name": "Temperature Sensor",
        "creator": "5a7d48755b225753411f6db5",
        "editor": "5a7d48755b225753411f6db5",
        "createdAt": ISODate("2018-04-16T17:39:29.978+07:00"),
        "updatedAt": ISODate("2018-04-16T17:39:29.978+07:00"),
        "__v": 0
    }
])