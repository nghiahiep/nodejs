const express = require('express')
const router = module.exports = express.Router()

router.post(
    '/v1/role',
    __auth.isPrivilege('ROLE_WRITE'),
    __roleController.create
)

router.get(
    '/v1/role',
    __auth.isPrivilege('ROLE_READ'),
    __roleController.reads
)

router.get(
    '/v1/role/:_id',
    __auth.isPrivilege('ROLE_READ'),
    __roleController.read
)

router.put(
    '/v1/role/:_id',
    __auth.isPrivilege('ROLE_WRITE', { checkPass: true }),
    __roleController.update
)

router.delete(
    '/v1/role/:_id',
    __auth.isPrivilege('ROLE_DELETE', { checkPass: true }),
    __roleController.delete
)

router.get(
    '/v1/role-select',
    __auth.isPrivilege('USER_READ'),
    __roleController.getForSelect2
)