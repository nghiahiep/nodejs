const express = require('express')
const router = module.exports = express.Router()

router.post(
    '/v1/sql/searchQLDulieu',
    __auth.isPrivilege('DEFAULT'),
    __posController.searchQLDulieu
)

router.post(
    '/v1/sql/rejectStage',
    __auth.isPrivilege('DEFAULT'),
    __posController.rejectStage
)

router.post(
    '/v1/sql/nextStage',
    __auth.isPrivilege('DEFAULT'),
    __posController.nextStage
)

router.post(
    '/v1/upFiles',
    __auth.isPrivilege('DEFAULT'),
    __posController.upFiles
)

router.post(
    '/v1/getFiles',
    __auth.isPrivilege('DEFAULT'),
    __posController.getFiles
)
