const express = require('express')
const router = module.exports = express.Router()

// router.post(
//     '/v1/ext',
//     __auth.isPrivilege('EXT_CREATE'),
//     __extController.create
// )

// router.get(
//     '/v1/ext',
//     __auth.isPrivilege('EXT_READ'),
//     __extController.reads
// )

// router.get(
//     '/v1/ext-user',
//     __auth.isPrivilege('EXT_READ'),
//     __extController.readByUser
// )

// router.get(
//     '/v1/ext/:_id',
//     __auth.isPrivilege('EXT_READ'),
//     __extController.read
// )

// router.put(
//     '/v1/ext/:_id',
//     __auth.isPrivilege('EXT_UPDATE', { checkPass: true }),
//     __extController.update
// )

// router.put(
//     '/v1/ext-user/:_id',
//     __auth.isPrivilege('EXT_UPDATE', { checkPass: true }),
//     __extController.updateUserExt
// )

// router.delete(
//     '/v1/ext/:_id',
//     __auth.isPrivilege('EXT_DELETE', { checkPass: true }),
//     __extController.delete
// )
router.post(
    '/v1/ext',
    __auth.isPrivilege('EXT_CREATE'),
    __extController.create
)

router.get(
    '/v1/ext',
    __auth.isPrivilege('DEFAULT'),
    __extController.reads
)

router.get(
    '/v1/ext-user',
    __auth.isPrivilege('DEFAULT'),
    __extController.readByUser
)

router.get(
    '/v1/ext/:_id',
    __auth.isPrivilege('DEFAULT'),
    __extController.read
)

router.put(
    '/v1/ext/:_id',
    __auth.isPrivilege('EXT_UPDATE', { checkPass: true }),
    __extController.update
)

router.put(
    '/v1/ext-user/:_id',
    __auth.isPrivilege('EXT_UPDATE', { checkPass: true }),
    __extController.updateUserExt
)

// router.put(
//     '/v1/ext-status/:_id',
//     __auth.isPrivilege('DEFAULT', { checkPass: true }),
//     __extController.updateStatusExt
// )

router.delete(
    '/v1/ext/:_id',
    __auth.isPrivilege('EXT_DELETE', { checkPass: true }),
    __extController.delete
)