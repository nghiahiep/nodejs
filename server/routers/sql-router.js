const express = require('express')
const router = module.exports = express.Router()

router.post(
    '/v1/sql/searchTSL',
    __auth.isPrivilege('DEFAULT'),
    __sqlController.searchTSL
)

router.post(
    '/v1/sql/deleteTSL',
    __auth.isPrivilege('DEFAULT'),
    __sqlController.deleteTSL
)

router.post(
    '/v1/sql/searchTeDrs',
    __auth.isPrivilege('DEFAULT'),
    __sqlController.searchTeDrs
)

router.post(
    '/v1/sql/executetedrs',
    __auth.isPrivilege('DEFAULT'),
    __sqlController.executetedrs
)

router.post(
    '/v1/sql/searchInforCust',
    __auth.isPrivilege('DEFAULT'),
    __sqlController.searchInforCust
)

router.post(
    '/v1/sql/searchInforDSAnew',
    __auth.isPrivilege('DEFAULT'),
    __sqlController.searchInforDSAnew
)

router.post(
    '/v1/sql/executeInforDSAnew',
    __auth.isPrivilege('DEFAULT'),
    __sqlController.executeInforDSAnew
)

// router.post(
//     '/v1/sql/executeScript',
//     __auth.isPrivilege('DEFAULT'),
//     __sqlController.executeScript
// )

router.get(
    '/v1/getUserFiles',
    __auth.isPrivilegeExFiles('DEFAULT'),
    __sqlController.getUserFiles
)

router.post(
    '/v1/downFiles',
    __auth.isPrivilegeExFiles('DEFAULT'),
    __sqlController.downFiles
)

router.post(
    '/v1/upFiles',
    __auth.isPrivilegeExFiles('DEFAULT'),
    __sqlController.upFiles
)

router.post(
    '/v1/sql/epCollNote',
    __auth.isPrivilegeExFiles('COLL'),
    __sqlController.epCollNote
)

router.get(
    '/v1/sql/epDRSDaily',
    __auth.isPrivilegeExFiles('DRS'),
    __sqlController.epDRSDaily
)

router.post(
    '/v1/sql/epUndRecord',
    __auth.isPrivilegeExFiles('UND'),
    __sqlController.epUndRecord
)

router.get(
    '/v1/sql/cicREQ',
    __auth.isPrivilegeExFiles('CIC'),
    __sqlController.cicREQ
)

router.post(
    '/v1/sql/cicRES',
    __auth.isPrivilegeExFiles('CIC'),
    __sqlController.cicRES
)

router.post(
    '/v1/sql/collDuedate',
    __auth.isPrivilegeExFiles('DUE'),
    __sqlController.collDuedate
)

router.get(
    '/v1/sql/dpdDaily',
    __auth.isPrivilegeExFiles('DPD'),
    __sqlController.dpdDaily
)

router.post(
    '/v1/sql/smsDaily',
    __auth.isPrivilegeExFiles('SMS'),
    __sqlController.smsDaily
)

router.post(
    '/v1/sql/penRecord',
    __auth.isPrivilegeExFiles('PEN'),
    __sqlController.penRecord
)
