const express = require('express')
const router = module.exports = express.Router()

// router.post(
//     '/v1/group',
//     __auth.isPrivilege('GROUP_CREATE'),
//     __groupController.create
// )

// router.get(
//     '/v1/group',
//     __auth.isPrivilege('GROUP_READ'),
//     __groupController.reads
// )

// router.get(
//     '/v1/group/:_id',
//     __auth.isPrivilege('GROUP_READ'),
//     __groupController.read
// )

// router.put(
//     '/v1/group/:_id',
//     __auth.isPrivilege('GROUP_UPDATE', { checkPass: true }),
//     __groupController.update
// )

// router.delete(
//     '/v1/group/:_id',
//     __auth.isPrivilege('GROUP_DELETE', { checkPass: true }),
//     __groupController.delete
// )

// router.get(
//     '/v1/group-select',
//     __auth.isPrivilege('DEFAULT'),
//     __groupController.getForSelect2
// )
router.post(
    '/v1/group',
    __auth.isPrivilege('GROUP_CREATE'),
    __groupController.create
)

router.get(
    '/v1/group',
    __auth.isPrivilege('DEFAULT'),
    __groupController.reads
)

router.get(
    '/v1/group/:_id',
    __auth.isPrivilege('DEFAULT'),
    __groupController.read
)

router.put(
    '/v1/group/:_id',
    __auth.isPrivilege('GROUP_UPDATE', { checkPass: true }),
    __groupController.update
)

router.delete(
    '/v1/group/:_id',
    __auth.isPrivilege('GROUP_DELETE', { checkPass: true }),
    __groupController.delete
)

router.get(
    '/v1/group-select',
    __auth.isPrivilege('DEFAULT'),
    __groupController.getForSelect2
)