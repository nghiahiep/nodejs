__app.use(require('./group-router'))
__app.use(require('./privilege-router'))
__app.use(require('./role-router'))
__app.use(require('./user-router'))
__app.use(require('./ext-router'))
__app.use(require('./sql-router'))

__app.use((req, res, next) => {
    res.send(`
        <div style="text-align:center">
            <h1>Api Not Found</h1>
            <a href="http://vnwalls.com">VnWalls</a>
        </div>
    `)
})
__app.use((err, req, res, next) => {
    if (err.message.search('Unexpected string in JSON') != -1) {
        return __response.fail(res, 'Data format invalid. Content type must be json or urlencoded')
    }
    return next(err)
})