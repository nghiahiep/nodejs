const express = require('express')
const router = module.exports = express.Router()


router.post('/v1/user',__auth.isPrivilege('USER_CREATE'),__userController.create)

router.get('/v1/user',__auth.isPrivilege('DEFAULT'),__userController.reads)

router.get('/v1/user/:_id',__auth.isPrivilege('DEFAULT'),__userController.read)

router.put('/v1/user/:_id',__auth.isPrivilege('USER_UPDATE', { checkPass: true }),__userController.update)

router.delete('/v1/user/:_id',__auth.isPrivilege('USER_DELETE', { checkPass: true }),__userController.delete)

router.get('/v1/user-select',__auth.isPrivilege('DEFAULT'),__userController.getForSelect2)

router.put('/v1/user/change-group/:_id',__auth.isPrivilege('USER_UPDATE'),__userController.changGroup)

router.put('/v1/user/change-password/:_id',__auth.isPrivilege('USER_UPDATE'),__userController.changePassword)

router.put('/v1/user/reset-password/:_id',__auth.isPrivilege('USER_UPDATE', { checkPass: true }),__userController.resetPassword)

router.post('/v1/login',__userController.login)

router.post('/v1/auth/login',__userController.authLogin)

router.get('/v1/auth/user',__auth.isPrivilege('DEFAULT'),__userController.authUser)
