const express = require('express')
const router = module.exports = express.Router()

router.post(
    '/v1/privilege'
    ,__auth.isPrivilege('PRIVILEGE_WRITE')
    ,__privilegeController.create
)
router.get(
    '/v1/privilege'
    ,__auth.isPrivilege(['PRIVILEGE_READ', 'ROLE_READ'])
    ,__privilegeController.reads
)
router.get(
    '/v1/privilege/:_id'
    ,__auth.isPrivilege('PRIVILEGE_READ')
    ,__privilegeController.read
)
router.delete(
    '/v1/privilege/:_id'
    ,__auth.isPrivilege('PRIVILEGE_DELETE', { checkPass: true })
    ,__privilegeController.delete
)