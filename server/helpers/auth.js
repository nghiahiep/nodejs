exports.isPrivilege = (privileges, option) => {
    return (req, res, next) => {
        __redis.get('token_' + req.headers.token, (err, user) => {
            if (err) {
                return __response.err(err, next)
            }

            if (!user) {
                return __response.tokenExpired(res)
            }

            req.userSession = user

            // if (__utils.isObject(option)) {
            //     if (option.checkPass) {
            //         req.password = req.body.password || req.headers.password

            //         if (!req.password) {
            //             return __response.fail(res, "Please provide your password")
            //         }


            //         if (!__passwordHash.verify(req.password + req.userSession.salt, req.userSession.password)) {
            //             return __response.fail(res, "Password invalid")
            //         }
            //     }
            // }

            __group.read({ _id: req.userSession.groupDefault }, 'groupChildrens')
                .then(group => {
                    if (!group) {
                        return __response.fail(res, "you don't have group owner")
                    }

                    __group.distinct({ groupChildrens: req.userSession.groupDefault }, '_id')
                        .then(groupParents => {
                            req.userSession.groupParents = [req.userSession.groupDefault].concat(groupParents)
                            req.userSession.groupChildrens = [req.userSession.groupDefault].concat(group.groupChildrens || [])
                            __utils.filter(req)

                            if (req.body.groupOwner && req.userSession.groupChildrens.indexOf(req.body.groupOwner) == -1) {
                                return __response.fail(res, "Group owner invalid")
                            }

                            if (req.userSession._id == process.env.USER_ADMIN) {
                                return next()
                            }
                            
                            if (req.userSession.type == 'ADMIN') {
                                return next()
                            }
                            
                            if (__utils.isString(privileges)) {
                                privileges = [privileges]
                            }

                            if (__utils.isArray(privileges)) {
                                for (let i in privileges) {
                                    let privilege = privileges[i]

                                    if (privilege == 'DEFAULT') {
                                        return next()
                                    }
                                    else if (__utils.isArray(req.userSession.privileges) && req.userSession.privileges.indexOf(privilege) != -1) {
                                        return next()
                                    }
                                }
                            }

                            return __response.permissionDenied(res)
                        })
                })
                .catch(err => {
                    return __response.err(err, next)
                })
        })
    }
}

exports.isPrivilegeExFiles = (privileges, option) => {
    return (req, res, next) => {
        __redis.get('token_' + req.headers.token, (err, user) => {
            // if (err) {
            //     return __response.err(err, next)
            // }

            // if (!user) {
            //     return __response.tokenExpired(res)
            // }

            req.userSession = user

            if (__utils.isString(privileges)) {
                if (privileges == 'DEFAULT') {
                    return next()
                } else {
                    let roles = req.userSession.roleFiles
                    let permission = false
                    if (req.userSession._id == process.env.USER_ADMIN) {
                        permission = true
                    }
                    // console.log(roles)
                    if (roles != undefined) {
                        roles.forEach((role) => {
                            if (role == privileges) {
                                permission = true
                            }
                        })
                    }
                    if (permission) {
                        return next()
                    } else {
                        return __response.permissionDenied(res)
                    }
                }
            }
        })
    }
}