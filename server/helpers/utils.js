const cryptoJS = require("crypto-js")
const randomString = require('randomstring')

exports.random = (opts) => {
    return opts ? randomString.generate(opts) : randomString.generate()
}

exports.isString = (str) => {
    return typeof str == 'string' ? true : false
}

exports.isNumber = (num) => {
    return typeof num == 'number' && !isNaN(num) ? true : false
}

exports.isBoolean = (boo) => {
    return typeof boo == 'boolean' ? true : false
}

exports.isArray = (arr) => {
    return Array.isArray(arr) ? true : false
}

exports.isObject = (obj) => {
    return typeof obj == 'object' && obj && Object.keys(obj).length > 0 && !this.isArray(obj) ? true : false
}

exports.isFunction = (func) => {
    return typeof func == 'function' ? true : false
}

exports.validation = (verify, data) => {
    if (!this.isObject(verify)) verify = {}
    if (!this.isObject(data)) data = {}

    let fail = {}
    let ok = {}

    for (let i in verify) {
        if (this.isObject(verify[i])) {
            switch (verify[i].type) {
                case String:
                    if (this.isString(data[i])) {
                        if (data[i].trim() == '' && verify[i].required == true) fail[i] = 'is empty'
                        else ok[i] = data[i]
                    }
                    else if (verify[i].required == true) fail[i] = 'is required string'
                    break
                case Number:
                    if (this.isNumber(data[i])) ok[i] = data[i]
                    else if (verify[i].required == true) fail[i] = 'is required number'
                    break
                case Boolean:
                    if (this.isBoolean(data[i])) ok[i] = data[i]
                    else if (verify[i].required == true) fail[i] = 'is required boolean'
                    break
                case Array:
                    if (this.isArray(data[i])) {
                        if (!data[i].length && verify[i].required == true) fail[i] = 'is empty'
                        else if (verify[i].typeChild) {
                            data[i].forEach((item, idx) => {
                                switch (verify[i].typeChild) {
                                    case String:
                                        if (this.isString(item)) {
                                            if (!this.isArray(ok[i])) ok[i] = []
                                            ok[i].push(item)
                                        }
                                        else if (verify[i].required == true) {
                                            if (!this.isArray(fail[i])) fail[i] = []
                                            fail[i].push(`${idx}: is required string`)
                                        }
                                        break
                                    case Number:
                                        if (this.isNumber(item)) {
                                            if (!this.isArray(ok[i])) ok[i] = []
                                            ok[i].push(item)
                                        }
                                        else if (verify[i].required == true) {
                                            if (!this.isArray(fail[i])) fail[i] = []
                                            fail[i].push(`${idx}: is required number`)
                                        }
                                        break
                                    case Boolean:
                                        if (this.isBoolean(item)) {
                                            if (!this.isArray(ok[i])) ok[i] = []
                                            ok[i].push(item)
                                        }
                                        else if (verify[i].required == true) {
                                            if (!this.isArray(fail[i])) fail[i] = []
                                            fail[i].push(`${idx}: is required boolean`)
                                        }
                                        break
                                    case Array:
                                        if (this.isArray(item)) {
                                            if (!this.isArray(ok[i])) ok[i] = []
                                            ok[i].push(item)
                                        }
                                        else if (verify[i].required == true) {
                                            if (!this.isArray(fail[i])) fail[i] = []
                                            fail[i].push(`${idx}: is required array`)
                                        }
                                        break
                                    case Object:
                                        if (this.isObject(item)) {
                                            if (this.isObject(verify[i].object)) {
                                                let out = this.validation(verify[i].object, item)

                                                if (!this.isArray(ok[i])) ok[i] = []
                                                ok[i].push(out.ok)

                                                if (this.isObject(out.fail) && verify[i].required == true) {
                                                    if (!this.isArray(fail[i])) fail[i] = []
                                                    let temp = {}
                                                    temp[idx] = out.fail
                                                    fail[i].push(temp)
                                                }
                                            }
                                            else {
                                                if (!this.isArray(ok[i])) ok[i] = []
                                                ok[i].push(item)
                                            }
                                        }
                                        else if (verify[i].required == true) {
                                            if (!this.isArray(fail[i])) fail[i] = []
                                            fail[i].push(`${idx}: is required object`)
                                        }
                                        break
                                }
                            })
                        }
                        else ok[i] = data[i]
                    }
                    else if (verify[i].required == true) fail[i] = 'is required array'
                    break
                case Object:
                    if (this.isObject(data[i])) {
                        if (this.isObject(verify[i].object)) {
                            let out = this.validation(verify[i].object, data[i])
                            ok[i] = out.ok

                            if (this.isObject(out.fail) && verify[i].required == true) fail[i] = out.fail
                        }
                        else ok[i] = data[i]
                    }
                    else if (verify[i].required == true) fail[i] = 'is required object'
                    break
            }
        }
    }

    return { fail, ok }
}

exports.filter = (req) => {
    req._skip = Number(req.query.skip) || 0
    req._limit = Number(req.query.limit) || 10

    req._sort = this.isObject(req.query.sort) ? req.query.sort : { createdAt: 'desc' }
    for (let i in req._sort) {
        if (!this.isString(req._sort[i]) || ['asc', 'desc'].indexOf(req._sort[i].toLowerCase()) == -1) {
            delete req._sort[i]
        }
    }

    req._query = { $and: [{}] }

    let search = this.isObject(req.query.search) ? req.query.search : {}
    for (let i in search) {
        if (this.isString(search[i])) {
            let temp = {}
            temp[i] = { $regex: search[i].replace(/(\s|-|:|_)+/g, '.*'), $options: 'i' }
            req._query['$and'].push(temp)
        }
    }

    let filter = this.isObject(req.query.filter) ? req.query.filter : {}
    for (let i in filter) {
        if (this.isString(filter[i])) {
            let temp = {}
            temp[i] = { $in: filter[i].split(',') }
            req._query['$and'].push(temp)
        }
    }

    if (req.query.type) {
        if (!req.query.arraySelected) {
            req.query.arraySelected = ''
        }

        if (req.query.type == 'from') {
            let objFrom = {}
            objFrom[req.query.keySelected] = { $nin: req.query.arraySelected.split(',') }
            req._query['$and'].push(objFrom)
        }
        else if (req.query.type == 'to') {
            let objTo = {}
            objTo[req.query.keySelected] = { $in: req.query.arraySelected.split(',') }
            req._query['$and'].push(objTo)
        }
    }
}

exports.copy = (data) => {
    return JSON.parse(JSON.stringify(data))
}

exports.dateNow = () => {
    return new Date()
}

exports.dateNowTimeSpan = () => {
    return new Date().getTime()
}

exports.toSlug = (str) => {
    str = str.toLowerCase()
    // remove diacritic marks
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a')
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e')
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i')
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o')
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u')
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y')
    str = str.replace(/(đ)/g, 'd')
    str = str.replace(/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/g, 'A')
    str = str.replace(/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/g, 'E')
    str = str.replace(/(Ì|Í|Ị|Ỉ|Ĩ)/g, 'I')
    str = str.replace(/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/g, 'O')
    str = str.replace(/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/g, 'U')
    str = str.replace(/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/g, 'Y')
    str = str.replace(/(Đ)/g, 'D')
    // remove special leter
    str = str.replace(/([^0-9A-Za-z-\s])/g, '')
    // replace white pace by -
    str = str.replace(/(\s+)/g, '')
    // remove - on top
    str = str.replace(/^-+/g, '')
    // remove - on tail
    str = str.replace(/-+$/g, '')
    // return
    return str
}



exports.encrypt = (string, key) => {
    return cryptoJS.AES.encrypt(string, key).toString()
}

exports.decrypt = (ciphertext, key) => {
    let bytes = cryptoJS.AES.decrypt(ciphertext, key)
    return bytes.toString(cryptoJS.enc.Utf8)
}