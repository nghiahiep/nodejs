exports.init = (app, workerId) => {
    __mongoose.connect(process.env.MONGO_URL)

    __mongoose.Promise = global.Promise

    __mongoose.connection.on('error', err => {
        if (workerId == 1) {
            app.set('ERROR_MONGO', err.message)
            console.log(`mongoDB: ${err.message}`)
        }
    })

    __mongoose.connection.on('connected', () => {
        if (workerId == 1) {
            app.set('ERROR_MONGO', null)
            console.log('mongoDB connected')
        }
    })

    __mongoose.connection.on('reconnected', () => {
        if (workerId == 1) {
            app.set('ERROR_MONGO', null)
            console.log('mongoDB reconnected')
        }
    })
    
    __mongoose.connection.on('disconnected', () => {
        __mongoose.connect(process.env.MONGO_URL)
    })
}