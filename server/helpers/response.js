exports.success = (res, obj) => {
    return res.status(200).json({ status: true, message: 'success', data: obj || {} })
}

exports.fail = (res, message, obj) => {
    return res.status(200).json({ status: false, message: message, err: obj || {} })
}

exports.tokenExpired = (res) => {
    return res.status(200).json({ status: false, code: 'TOKEN_EXPIRED', message: 'token is expired', err: {} })
}

exports.permissionDenied = (res) => {
    return this.fail(res, 'permission denied')
}

exports.invalidData = (res, obj) => {
    return this.fail(res, 'data invalid', obj)
}

exports.create = (res, obj) => {
    return res.status(201).json({ status: true, message: 'success', data: obj || {} })
}

exports.exits = (res, model) => {
    return this.fail(res, `${model} exits`)
}

exports.dontExits = (res, model) => {
    return this.fail(res, `${model} don't exits`)
}

exports.reads = (res, list, skip, limit, total) => {
    return res.status(200).json({ status: true, message: 'success', data: list, skip: skip, limit: limit, total: total })
}