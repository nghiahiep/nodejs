// GLOBAL LIBRARY
__fs = require('fs')
__qr = require('qr-image')
__moment = require('moment')
__passwordHash = require('password-hash')
__multer = require('multer')({ dest: __dirname + '/../uploads/', limits: { fileSize: 2000000000 } }).any()
__log = require('./debug')('debug', 'log')
__utils = require('./utils')
__response = require('./response')
__auth = require('./auth')
__formidable = require('formidable')

// SQLSERVER
__connection = require('tedious').Connection
__requestsql = require('tedious').Request
__types = require('tedious').TYPES
__sqlserverCofig = {  
    userName: process.env.NODE_SQLDB_USER,
    password: process.env.NODE_SQLDB_PASSWORD,  
    server: process.env.NODE_SQLDB_CONNECTIONURL, 
    options: {encrypt: true, database: process.env.NODE_SQLDB_DATABASE}  
}

// SCP
__client = require('ssh2').Client
__sftpConfigSQL = {
    host: '192.168.105.199',
    port: '22',
    username: 'mafc1046',
    password: 'abc123$'
}
__pathRpSQL = '/REPORT/Accounting/'
__sftpConfigORA = {
    host: '192.168.100.20',
    port: '22',
    username: 'root',
    password: 'root123'
}
__pathRpORA = '/u01/MAFC_REPORTS/'
__pathRpCIC = '/data02/MIRAE/'

__pathFiles = './exportFiles/'


// ORACLE
__oracledb = require('oracledb')
__oracleConfig = require('./oracleconfig')
__oracleConfigMC = require('./oracleconfigMACAS')
__oracleConfigMM = require('./oracleMAMAS')
__oracleAPI = require('./oracle')
// __oracleAPI.serverOracle()

// MONGODB
__mongoose = require('mongoose')
__schema = __mongoose.Schema
__mongodb = require('./mongodb')
__mongodb.init(__app, __workerId)

// CACHE
__redis = require('./redis')
__redis.init(__app, __workerId)

// // FTP
// __ftp = require('./ftp')
// __ftp.init(__app, __workerId)

// SOCKETIO
__socketio = require('./socketio')
__socketNameSpace = __socketio.init(__server).socketNameSpace
// __socketExt = __socketio.init(__server).socketExt

// // MOSCA
// __mosca = require('./mosca')
// __mosca.init(__app, __workerId)

// // KAFKA
// __kafka = require('./kafka')
// __kafka.init(__app, __workerId)

// MIDDLEWARE
__middleware = require('./middleware')
__middleware.init(__app, __express)

