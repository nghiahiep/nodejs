exports.init = (app, express) => {
    app.set('views', __dirname + '/views')
    app.set('view engine', 'ejs')
    app.set('json spaces', 4)
    app.set('trust proxy', 1)
    app.use(require('body-parser').urlencoded({ limit: '50mb', extended: true }))
    app.use(require('body-parser').json({ limit: '50mb' }))
    app.use(require('cookie-parser')())
    app.use(require('express-session')({ secret: "ljX1ft18RdaiiVzGbKlQ1HsDJfcJkSKE", resave: false, saveUninitialized: true, cookie: { secure: true } }))
    app.use(require('morgan')('dev'))
    app.use(require('helmet')())
    app.use(require('compression')())
    app.use(require('cors')())
    app.use(require('serve-favicon')(__dirname + '/../files/favicon.png'))
    app.use(express.static(__dirname + '/../public/'))

    app.use((req, res, next) => {
        if (app.get('ERROR_MONGO') || app.get('ERROR_REDIS') || app.get('ERROR_FTP') || app.get('ERROR_KAFKA')) {
            let err = new TypeError(`
                mongo: ${app.get('ERROR_MONGO')}
                redis: ${app.get('ERROR_REDIS')}
                ftp: ${app.get('ERROR_FTP')}
                kafka: ${app.get('ERROR_KAFKA')}
            `)
            return next(err)
        }
        return next()
    })
}