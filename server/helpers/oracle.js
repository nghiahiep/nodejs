// exports.serverOracle = () => {
//     __oracledb.autoCommit = false;
//     __oracledb.getConnection(
//     {
//         user          : __oracleConfig.user,
//         password      : __oracleConfig.password,
//         connectString : __oracleConfig.connectString
//     },
//     function(err, connection) {
//         if (err) {
//           console.log('err ' + err.message)
//           return
//         }
//         connection.execute(
//             `BEGIN SP_CIC_REQUEST_MAIN; END;`,
//             // {  // bind variables
//             //     agreeid: 10001,
//             //     v_amtfin: { dir: __oracledb.BIND_OUT, type: __oracledb.NUMBER, maxSize: 40 },
//             //     v_tenure: { dir: __oracledb.BIND_OUT, type: __oracledb.NUMBER, maxSize: 40 },
//             //     custid: { dir: __oracledb.BIND_OUT, type: __oracledb.NUMBER, maxSize: 40 },
//             //     v_insfee: { dir: __oracledb.BIND_OUT, type: __oracledb.NUMBER, maxSize: 40 }
//             // },
//             function (err, result) {
//                 if (err) { console.log(err.message); return; }
//                 console.log(result.outBinds);
//             }
//         );
//     })

//     function doRelease(connection) {
//       connection.close(
//         function(err) {
//           if (err) {
//             console.log(err.message);
//           }
//         });
//     }
// }

// exports.create = (req, res, next) => {
//     let validation = __utils.validation({
//         groupOwner: { type: String, required: true },
//         ext: { type: String, required: true },
//         password: { type: String, required: true },
//         host: { type: String, required: true }
//     }, req.body)

//     if (__utils.isObject(validation.fail)) {
//         return __response.invalidData(res, validation.fail)
//     }

//     let object = { ...validation.ok, creator: req.userSession._id, editor: req.userSession._id, createdAt: __utils.dateNow(), updatedAt: __utils.dateNow() }

//     __ext.create(object)
//         .then(docs => {
//             return __ext.read({ _id: docs._id })
//                 .populate('editor', 'firstName lastName')
//                 .populate('groupOwner', 'name')
//                 .populate('user', 'user')
//                 .then(docs => {
//                     __extRedisController.refresh()
//                     return __response.success(res, docs)
//                 })
//         })
//         .catch(err => {
//             if (err.code == 11000) {
//                 return __response.exits(res, 'User')
//             }
//             return __response.err(err, next)
//         })
// }
