exports.init = (server) => {
    const io = require('socket.io')(server)

    io.engine.generateId = (req) => { return req._query.token }

    // io.adapter(require('socket.io-redis')({ host: process.env.REDIS_HOST, port: process.env.REDIS_PORT, auth_pass: process.env.REDIS_PASS }))
    // io.of('/').adapter.on('error', err => { })

    const socketNameSpace = io.of('/')
    socketNameSpace.adapter.on('error', (err) => { })
    socketNameSpace.on('connection', (socket) => {
        __redis.get('token_' + socket.request._query.token, (err, user) => {
            if (user) {
                var extTempt = {}
                socket.emit('auth', { status: true, message: 'Success' })
                __log(socket.id + ' register')
                // serverOracle()
                // socket.on('socket_updateStatus', (data) => {
                //     if (!!data.ext) {
                //         updateStatus(data, socket, io, socketNameSpace)
                //     }
                //     // updateRoom(data, socket)
                // })
                socket.on('socket_getAll', (data) => {
                    let nickname = socket.request._query.token
                    updateRoom(nickname, data, socket, io, socketNameSpace)
                })
                socket.on('socketupdateStatus', (data) => {
                    updateExtRedis(data, socketNameSpace)
                    extTempt = data
                    // socket.emit('auth', { status: false, message: 'Token invalid' })
                })
                socket.on('disconnect', () => {
                    __log(socket.id + ' disconnect')
                    //let obj = JSON.parse(extTempt)
                    //console.log(obj.ext)
                    //if (obj.ext != undefined ) {
                        //updateExtRedis(obj, socketNameSpace, true, socket)
                    //}
                })
            }
            else {
                socket.emit('auth', { status: false, message: 'Token invalid' })
            }
        })
    })
    
    // const socketExt = io.of('/')
    // socketExt.adapter.on('error', (err) => { })
    // socketExt.on('connection', (socket) => {
    //     // console.log(socket.request._query.token)
    //     socket.on('socketupdateStatus', (data) => {
    //         updateStatusExt(data, socket, io)
    //         // console.log(data)
    //     })
    //     // socket.emit('servercommand', 'shut up')
    // })
    return { socketNameSpace }
}  

const updateExtRedis = (data, socketNameSpace, logout, socket) => {
    let obj = JSON.parse(data)
    let status = ''
    let phone = ''
    if (logout == true) {
        status = 'Offline'
        phone = '---------'
    } else {
        status = obj.status
        phone = obj.phone
    }
        
    let ext = obj.ext
    
    let nameRoom = 'extRoom_' + ext.groupOwner.name
    if (logout == true) {
        socket.join(nameRoom)
    }
    
    __redis.get(nameRoom, (err, roomsRedis) => {
        for (let i in roomsRedis) {
            if(roomsRedis[i].ext === ext.ext) {
                let dataUpdated = Object.assign(ext, { status: status }, { phone: phone }, { timeUpdated: __utils.dateNowTimeSpan() })
                roomsRedis[i] = dataUpdated
                __redis.set(nameRoom, roomsRedis)
                socketNameSpace.in(nameRoom).emit('updatedext', dataUpdated)
            }
        }
        // console.log(roomsRedis)
    })
}

const updateRoom = (nickname, data, socket, io, socketNameSpace) => {
    let dataGetAll = []
    for (let j = 0; j < data.rooms.length + 1; j++) {
        let nameRoom = 'extRoom_' + data.rooms[j]
        socket.nickname = nickname
        socket.join(nameRoom)
        //console.log(nameRoom)
        __redis.get(nameRoom, (err, ext) => {
            //console.log(ext)
            if (j === data.rooms.length) {
            //console.log(dataGetAll)
                socket.emit('broadcastgetall', dataGetAll)
            }
            if (err || ext != null) {
                let roomData = {
                    group: data.rooms[j],
                    timeSystem: __utils.dateNowTimeSpan(),
                    users: ext
                }
                dataGetAll.push(roomData) 
                //console.log(data.rooms.length)
            }
        })
    }
}

const updateStatus = (data, socket, io, socketNameSpace) => {
    let nameRoom = 'extRoom_' + data.ext.groupOwner.name
    let pushNewExt = true
    let extRoom = []
    let extUpdated = Object.assign(data.ext, data.status, { timeUpdated: __utils.dateNowTimeSpan() })
    __redis.get(nameRoom, (err, ext) => {
        if (err || !ext) {
            extRoom = [ Object.assign(data.ext, data.status) ]
        } else {
            for (let i = 0; i < ext.length; i++) {
                if(ext[i]._id === data.ext._id) {
                    pushNewExt = false
                    ext[i] = Object.assign(data.ext, data.status, { timeUpdated: __utils.dateNowTimeSpan() })
                }
            }
            if (pushNewExt) {
                ext.push(Object.assign(data.ext, data.status, { timeUpdated: __utils.dateNowTimeSpan() }))
            }
            extRoom = ext
        }
        __redis.set(nameRoom, extRoom)
        let roomData = {
            group: data.ext.groupOwner.name,
            timeSystem: __utils.dateNowTimeSpan(),
            users: [extUpdated]
        }
        socketNameSpace.in(nameRoom).emit('broadcast', roomData)
    })
}

exports.emitMachineStatusMessage = (object) => {
    let objRes = { code: null, type: null, option: null, uri: null, _id: null, lastTouch: __utils.dateNow(), module: null, moduleMessage: null }
    Object.assign(objRes, object)
    __socketNameSpace.emit(`${objRes._id}/machine/status`, objRes)
}

/////////////////////////////////////// MOBILE ///////////////////////////////////////////////////

exports.emitMobileMachineStatusMessage = (object) => {
    let objRes = { socketId: null, _id: null, status: null, createdAt: __utils.dateNow() }
    Object.assign(objRes, object)
    __socketNameSpace.to(`/socket#${objRes.socketId}`).emit('mobile/machine/status', objRes)
}

exports.emitMobileMachineUpdateFloor = (object) => {
    let objRes = { socketId: null, _id: null, floor: null, statusFloor: null }
    Object.assign(objRes, object)
    __socketNameSpace.to(`/socket#${objRes.socketId}`).emit('mobile/machine/updatefloor', objRes)
}

exports.emitMobileProductAdd = (object) => {
    let objRes = { _id: null, name: null, price: null, discount: null, urlImages: null, description: null, quantity: null, updatedAt: __utils.dateNow() }
    Object.assign(objRes, object)
    __socketNameSpace.emit('mobile/product/add', objRes)
}

exports.emitMobileProductUpdate = (object) => {
    let objRes = { _id: null, name: null, price: null, discount: null, urlImages: null, description: null, quantity: null, updatedAt: __utils.dateNow() }
    Object.assign(objRes, object)
    __socketNameSpace.emit('mobile/product/update', objRes)
}