exports.serverSql = () => {
    let config = {  
        userName: process.env.NODE_SQLDB_USER,
        password: process.env.NODE_SQLDB_PASSWORD,  
        server: process.env.NODE_SQLDB_CONNECTIONURL, 
        options: {encrypt: true, database: process.env.NODE_SQLDB_DATABASE}  
    }
    let connection = new __connection(config)
    connection.on('connect', function(err) {
        console.log("Connected")
        executeStatement()
    })
    const executeStatement = () => {  
        request = new __requestsql("select top 1 * from TeleSales_Leads", function(err) {  
        if (err) {  
            console.log(err)}  
        });  
        let result = ""
        request.on('row', function(columns) {  
            columns.forEach(function(column) {  
              if (column.value === null) {  
                console.log('NULL') 
              } else {  
                result+= column.value + " " 
              }  
            });  
            console.log(result) 
            result =""
        });  

        request.on('done', function(rowCount, more) {  
        console.log(rowCount + ' rows returned')
        });  
        connection.execSql(request)
    }
}

