const redis = require("redis").createClient({ host: process.env.REDIS_HOST, port: process.env.REDIS_PORT, password: process.env.REDIS_PASS })

exports.init = (app, workerId) => {
    redis.on('error', err => {
        if (workerId == 1) {
            app.set('ERROR_REDIS', err.message)
            console.log(`redis: ${err.message}`)
        }
    })

    redis.on('ready', () => {
        if (workerId == 1) {
            app.set('ERROR_REDIS', null)
            console.log('redis connected')
        }
    })
}

exports.set = (key, value, callback) => {
    redis.set(key, JSON.stringify(value), (err, reply) => {
        if (__utils.isFunction(callback)) {
            return callback(err, reply)
        }
    })
}

exports.get = (key, callback) => {
    redis.get(key, (err, reply) => {
        if (__utils.isFunction(callback)) {
            try {
                reply = JSON.parse(reply)
                return callback(err, reply)
            }
            catch (err) {
                return callback(err, reply)
            }
        }
    })
}

exports.del = (key, callback) => {
    redis.del(key, (err, reply) => {
        if (__utils.isFunction(callback)) {
            try {
                reply = JSON.parse(reply)
                return callback(err, reply)
            }
            catch (err) {
                return callback(err, reply)
            }
        }
    })
}