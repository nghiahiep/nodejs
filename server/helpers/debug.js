const util = require('util')

module.exports = (set, opt) => {
    return function () {
        if (set == process.env.NODE_DEBUG) {
            switch (opt) {
                case 'info':
                    console.info(`[${opt.toUpperCase()}-${__moment().format('YYYYMMDDHHmmss')}] ${util.format.apply(util, arguments)}`, '\n')
                    break
                case 'error':
                    console.error(`[${opt.toUpperCase()}-${__moment().format('YYYYMMDDHHmmss')}] ${util.format.apply(util, arguments)}`, '\n')
                    break
                case 'warn':
                    console.warn(`[${opt.toUpperCase()}-${__moment().format('YYYYMMDDHHmmss')}] ${util.format.apply(util, arguments)}`, '\n')
                    break
                case 'log':
                    console.log(`[${opt.toUpperCase()}-${__moment().format('YYYYMMDDHHmmss')}] ${util.format.apply(util, arguments)}`, '\n')
                    break
                default:
                    console.log(`[${set.toUpperCase()}-${__moment().format('YYYYMMDDHHmmss')}] ${util.format.apply(util, arguments)}`, '\n')
            }
        }
    }
}