exports.create = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        user: { type: String, required: true },
        password: { type: String, required: true },
        type: { type: String, required: true },
        role: { type: String },
        firstName: { type: String },
        lastName: { type: String },
        phone: { type: String },
        urlImage: { type: String },
        roleFiles: { type: Array }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }
    let object = { ...validation.ok, creator: req.userSession._id, editor: req.userSession._id, createdAt: new Date(), updatedAt: new Date() }
    object.token = __utils.random()
    object.salt = __utils.random()
    object.password = __passwordHash.generate(object.password + object.salt)
    __user.create(object)
        .then(docs => {
            return __user.read({ _id: docs._id })
                .populate('editor', 'firstName lastName')
                .populate('groupOwner', 'name')
                .then(docs => {
                    return __response.success(res, docs)
                })
        })
        .catch(err => {
            if (err.code == 11000) {
                return __response.exits(res, 'User')
            }
            return __response.err(err, next)
        })
}

exports.reads = (req, res, next) => {
    req._limit = 1000
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: { $nin: [process.env.USER_ADMIN, req.userSession._id] } })

    __user.reads(req._query, req._skip, req._limit, req._sort, '-salt -password')
        .populate('editor', 'firstName lastName user')
        .populate('groupOwner', 'name')
        .then(list => {
            return __user.count(req._query)
                .then(total => {
                    return __response.reads(res, list, req._skip, req._limit, total)
                })
        })
        .catch(err => {
            return next(err)
        })
}

exports.read = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __user.read(req._query, '-salt -password')
        .populate('editor', 'firstName lastName user')
        .populate('groupOwner', 'name')
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__user')
            }
            return __response.success(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}

exports.update = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        user: { type: String, required: true },
        type: { type: String, required: true },
        role: { type: String },
        firstName: { type: String },
        lastName: { type: String },
        phone: { type: String },
        urlImage: { type: String },
        roleFiles: { type: Array }
    }, req.body)
    
    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, editor: req.userSession._id, updatedAt: new Date() }

    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    //req._query['$and'].push({ _id: { $nin: [process.env.USER_ADMIN, req.userSession._id] } })
    req._query['$and'].push({ _id: req.params._id })

    __user.update(req._query, object)
        .populate('editor', 'firstName lastName')
        .populate('groupOwner', 'name')
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__user')
            }
            __redis.del('token_' + docs.token)
            return __response.success(res, Object.assign(docs, object))
        })
        .catch(err => {
            if (err.code == 11000) {
                return __response.exits(res, '__user')
            }
            return next(err)
        })
}

exports.delete = (req, res, next) => {
    if (req.params._id == process.env.USER_ADMIN) return __response.permissionDenied(res)

    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: { $nin: [process.env.USER_ADMIN, req.userSession._id] } })
    req._query['$and'].push({ _id: req.params._id })

    __user.delete(req._query)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__user')
            }

            __redis.del('token_' + docs.token)
            return __response.success(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}

exports.login = (req, res, next) => {
    let validation = __utils.validation({
        user: { type: String, required: true },
        password: { type: String, required: true }
    }, req.body)
    console.log(req.body)
    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = validation.ok

    __user.read({ user: object.user })
        .populate('role', 'privileges')
        .then(__user => {
            if (!__user) {
                return __response.fail(res, "user invalid")
            }

            if (!__passwordHash.verify(object.password + __user.salt, __user.password)) {
                return __response.fail(res, "password invalid")
            }

            let userCopy = __utils.copy(__user)
            userCopy.salt = undefined
            userCopy.privileges = []
            if (userCopy.role) {
                userCopy.privileges = userCopy.role.privileges
                userCopy.role = undefined
            }
            userCopy.groupDefault = userCopy.groupOwner
            __redis.set('token_' + userCopy.token, userCopy)
            // console.log(__user)
            
            return __response.success(res, userCopy)
        })
        .catch(err => {
            return next(err)
        })
}

exports.authLogin = (req, res, next) => {
    let validation = __utils.validation({
        user: { type: String, required: true },
        password: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = validation.ok
    console.log(req.body)
    __user.read({ user: object.user })
        .populate('role', 'privileges')
        .then(__user => {
            if (!__user) {
                return __response.fail(res, "Email invalid")
            }

            if (!__passwordHash.verify(object.password + __user.salt, __user.password)) {
                return __response.fail(res, "Password invalid")
            }

            let userCopy = __utils.copy(__user)
            userCopy.salt = undefined
            userCopy.privileges = []
            if (userCopy.role) {
                userCopy.privileges = userCopy.role.privileges
                userCopy.role = undefined
            }
            userCopy.groupDefault = userCopy.groupOwner
            __redis.set('token_' + userCopy.token, userCopy)
            // console.log(userCopy)
            // tao thu muc user
            // let dir = './exportFiles/' + userCopy._id

            // if (!__fs.existsSync(dir)){
            //     __fs.mkdirSync(dir)
            // }
            
            return __response.success(res, { token: userCopy.token })
        })
        .catch(err => {
            return next(err)
        })
}

exports.authUser = (req, res, next) => {
    __redis.get('token_' + req.headers.token, (err, user) => {
        if (err) return _response.error(res, err)
        return __response.success(res, user)
    })
}

exports.getForSelect2 = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })

    __user.reads(req._query, null, null, { name: 'asc' }, 'firstName lastName user')
        .then(list => {
            list = __utils.copy(list)
            list.forEach(item => {
                item.id = item._id
                item.text = (item.firstName || item.lastName ? item.firstName + ' ' + item.lastName : item.user)
            })
            return __response.success(res, list)
        })
        .catch(err => {
            return next(err)
        })
}

exports.changGroup = (req, res, next) => {
    let validation = __utils.validation({
        groupDefault: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = validation.ok
    req.userSession.groupDefault = object.groupDefault
    req.userSession.groupChildrens = undefined
    req.userSession.groupParents = undefined
    __redis.set('token_' + req.userSession.token, req.userSession)

    return __response.success(res, req.userSession)
}

exports.changePassword = (req, res, next) => {
    let validation = __utils.validation({
        passwordOld: { type: String, required: true },
        passwordNew: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = validation.ok

    __user.read({ _id: req.userSession._id })
        .populate('role', 'privileges')
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__user')
            }

            if (!__passwordHash.verify(object.passwordOld + docs.salt, docs.password)) {
                return __response.fail(res, "passwordOld invalid")
            }

            let objUpdate = {}
            objUpdate.salt = __utils.random()
            objUpdate.password = __passwordHash.generate(object.passwordNew + objUpdate.salt)

            return __user.update({ _id: req.userSession._id }, objUpdate)
                .then(__user => {
                    req.userSession.password = objUpdate.password
                    __redis.set('token_' + req.userSession.token, req.userSession)
                    return __response.success(res, req.userSession)
                })
        })
        .catch(err => {
            return next(err)
        })
}

exports.resetPassword = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })
    req._query['$and'].push({ _id: { $nin: [process.env.USER_ADMIN, req.userSession._id] } })

    __user.read(req._query)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__user')
            }

            let objUpdate = {}
            objUpdate.salt = __utils.random()
            objUpdate.password = __passwordHash.generate(docs.user + objUpdate.salt)
            objUpdate.editor = req.userSession._id
            objUpdate.updatedAt = new Date()

            return __user.update(req._query, objUpdate)
                .then(__user => {
                    __redis.del('token_' + __user.token)
                    return __response.success(res, __user)
                })
        })
        .catch(err => {
            return next(err)
        })
}
