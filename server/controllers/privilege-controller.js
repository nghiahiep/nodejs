exports.create = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        _id: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, creator: req.userSession._id, editor: req.userSession._id, createdAt: __utils.dateNow(), updatedAt: __utils.dateNow() }


    __privilege.create(object)
        .then(docs => {
            return __privilege.read({ _id: docs._id })
                .populate('editor', 'firstName lastName')
                .populate('groupOwner', 'name')
                .then(docs => {
                    return __response.success(res, docs)
                })
        })
        .catch(err => {
            if (err.code == 11000) {
                return __response.exits(res, 'User')
            }
            return __response.err(err, next)
        })
}

exports.reads = (req, res, next) => {
    if (req.userSession._id != process.env.USER_ADMIN) {
        req._query['$and'].push({ _id: { $in: req.userSession.privileges } })
    }

    __privilege.reads(req._query, req._skip, req._limit, req._sort)
        .then(list => {
            return __privilege.count(req._query)
                .then(total => {
                    return __response.reads(res, list, req._skip, req._limit, total)
                })
        })
        .catch(err => {
            return next(err)
        })
}

exports.read = (req, res, next) => {
    if (req.userSession._id != process.env.USER_ADMIN) {
        req._query['$and'].push({ _id: { $in: req.userSession.privileges } })
    }
    req._query['$and'].push({ _id: req.params._id })

    __privilege.delete(req._query)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'Privilege')
            }
            return __response.success(res, Object.assign(docs, object))
        })
        .catch(err => {
            return next(err)
        })
}

exports.delete = (req, res, next) => {
    if (req.userSession._id != process.env.USER_ADMIN) {
        req._query['$and'].push({ _id: { $in: req.userSession.privileges } })
    }
    req._query['$and'].push({ _id: req.params._id })

    __privilege.delete(req._query)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'Privilege')
            }
            return __response.success(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}