exports.create = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        ext: { type: String, required: true },
        password: { type: String, required: true },
        host: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, creator: req.userSession._id, editor: req.userSession._id, createdAt: __utils.dateNow(), updatedAt: __utils.dateNow() }

    __ext.create(object)
        .then(docs => {
            return __ext.read({ _id: docs._id })
                .populate('editor', 'firstName lastName')
                .populate('groupOwner', 'name')
                .populate('user', 'user')
                .then(docs => {
                    __extRedisController.refresh()
                    return __response.success(res, docs)
                })
        })
        .catch(err => {
            if (err.code == 11000) {
                return __response.exits(res, 'User')
            }
            return __response.err(err, next)
        })
}

exports.reads = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    __ext.reads(req._query)
    //__ext.reads(req._query, req._skip, req._limit, req._sort)
        .populate('editor', 'firstName lastName email')
        .populate('groupOwner', 'name')
        .populate('user', 'user')
        .then(list => {
            return __ext.count(req._query)
                .then(total => {
                    return __response.reads(res, list, req._skip, req._limit, total)
                })
        })
        .catch(err => {
            return __response.err(err, next)
        })
}

exports.readByUser = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ user: req.userSession._id })
    __ext.read(req._query)
        .populate('editor', 'firstName lastName email')
        .populate('groupOwner', 'name')
        .populate('user', 'user')
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'ext')
            }
            return __response.success(res, docs)
        })
        .catch(err => {
            return __response.err(err, next)
        })
}

exports.read = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __ext.read(req._query)
        .populate('editor', 'firstName lastName email')
        .populate('groupOwner', 'name')
        .populate('user', 'user')
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'ext')
            }
            return __response.success(res, docs)
        })
        .catch(err => {
            return __response.err(err, next)
        })
}

exports.update = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        ext: { type: String, required: true },
        password: { type: String, required: true },
        host: { type: String, required: true },
        user: { type: String }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, editor: req.userSession._id, updatedAt: __utils.dateNow() }
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })
    
    __ext.update(req._query, object)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'ext')
            }
            __extRedisController.refresh()
            return __response.success(res, Object.assign(docs, object))
        })
        .catch(err => {
            return __response.err(err, next)
        })
}

exports.updateUserExt = (req, res, next) => {
    let validation = __utils.validation({
        user: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, editor: req.userSession._id, updatedAt: __utils.dateNow() }

    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __ext.update(req._query, object)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'ext')
            }
            __extRedisController.refresh()
            return __response.success(res, Object.assign(docs, object))
        })
        .catch(err => {
            return __response.err(err, next)
        })
}

// exports.updateStatusExt = (req, res, next) => {
//     let validation = __utils.validation({
//         status: { type: String, required: true },
//         phone: { type: String }
//     }, req.body.data)

//     if (__utils.isObject(validation.fail)) {
//         return 'invalidData'
//     }

//     let object = { ...validation.ok, editor: req.userSession._id, updatedAt: __utils.dateNow() }
    
//     req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
//     req._query['$and'].push({ _id: req.body.id })
    
//     __ext.update(req._query, object)
//         .then(docs => {
//             if (!docs) {
//                 return 'dont Exits'
//             }
//             console.log(docs)
//             return 'success'
//         })
//         .catch(err => {
//             return 'err'
//         })
// }

exports.delete = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __ext.delete(req._query)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'ext')
            }
            __extRedisController.refresh()
            return __response.success(res, docs)
        })
        .catch(err => {
            return __response.err(err, next)
        })
}
