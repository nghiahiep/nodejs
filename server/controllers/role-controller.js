
exports.create = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        name: { type: String, required: true },
        privileges: { type: Array, required: true, typeChild: String }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, creator: req.userSession._id, editor: req.userSession._id, createdAt: __utils.dateNow(), updatedAt: __utils.dateNow() }

    __role.create(object)
        .then(docs => {
            return __role.read({ _id: docs._id })
                .populate('editor', 'firstName lastName')
                .populate('groupOwner', 'name')
                .then(docs => {
                    return __response.success(res, docs)
                })
        })
        .catch(err => {
            if (err.code == 11000) {
                return __response.exits(res, 'User')
            }
            return __response.err(err, next)
        })
}

exports.reads = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })

    __role.reads(req._query, req._skip, req._limit, req._sort)
        .populate('editor', 'firstName lastName email')
        .populate('groupOwner', 'name')
        .then(list => {
            return __role.count(req._query)
                .then(total => {
                    return __response.reads(res, list, req._skip, req._limit, total)
                })
        })
        .catch(err => {
            return next(err)
        })
}

exports.read = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __role.read(req._query)
        .populate('editor', 'firstName lastName email')
        .populate('groupOwner', 'name')
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__role')
            }
            return __response.success(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}

exports.update = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        name: { type: String, required: true },
        privileges: { type: Array, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, editor: req.userSession._id, updatedAt: __utils.dateNow() }
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __role.update(req._query, object)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__role')
            }

            __user.reads({ __role: docs._id }, null, null, null, 'token')
                .then(users => {
                    for (let i in users) {
                        redis.del('token_' + users[i].token)
                    }
                })

            return __response.success(res, Object.assign(docs, object))
        })
        .catch(err => {
            return next(err)
        })
}

exports.delete = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __role.delete(req._query)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, '__role')
            }

            User.reads({ __role: docs._id }, null, null, null, 'token')
                .then(users => {
                    for (let i in users) {
                        redis.del('token_' + users[i].token)
                    }
                })

            return __response.success(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}

exports.getForSelect2 = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })

    __role.reads(req._query, null, null, { name: 'asc' }, 'name')
        .then(list => {
            list = __utils.copy(list)
            list.forEach(item => {
                item.id = item._id
                item.text = item.name
            })
            return __response.success(res, list)
        })
        .catch(err => {
            return next(err)
        })
}