module.exports = `SELECT DISTINCT
    APP.LAA_PRODUCT_ID_C,
    sch.SCHEMEDESC,
    cust.OTHER_EMPLOYER_NAME_C COM_NAME,
    SYSDATE PENDING_DATE,
    CUST.CUSTOMERNAME CUSTOMERNAME,
    CUST.AGE,
    CUST.CIF_NO,
    CUST.PAN_NO,
    app.VC_ACCTOUNT_OFF_CD PIC_CODE,
    app.LAA_APP_SIGNED_DATE_D APP_DATE,
    cust.no_of_dependent NO_OF_DEPENDENT,
    DECODE (cust.sex, 'M', 'MALE', 'FEMALE') GENDER,
    cust.VC_ALIAS_NAME SOURCE,
    DECODE (cust.MARITAL_STATUS, 'S', 'Single', 'M', 'Married', 'W', 'Deviorced') MARITAL_STATUS,
    cust.spousename,
    DECODE (cust.qualification, 'U', 'University', 'LG', 'Lower High School', 'UU', 'Upper University', 'CE', 'COLLEGE OR EQUIVALENT', 'HG', 'High School', cust.qualification) EDUCATION,
    ROUND (cust.monthly_income, 0) INCOME,
    APP.APP_ID_C AGREEMENTID,
    DECODE (APP.LAA_PRODUCT_ID_C, 'SALPIL', 'Cash Loan', 'OCLPIL', 'Online Cash Loan', 'MAEALPIL', 'MA EMPLOYEE CASH LOAN', 'SELPIL', 'SELF-EMPLOYED CASH LOAN',APP.LAA_PRODUCT_ID_C) PRODUCT_NAME,
    ROUND (app.LAA_APP_REQ_AMTFIN_N, 0) LOAN_AMOUNT,
    ROUND (APP.LAA_APP_APPR_TERM_N, 0) TENURE,
    TRUNC (app.LAA_APP_REQ_IRR_N, 2) EFF_RATE,
    TRUNC (app.LAA_APP_REQ_INTRATE_N, 2) FLAT_RATE,
    app.LAA_APP_DUEDAY_C DUEDAY,
    ROUND (app.LAA_EMI_N, 0) EMI,
    cust.makerid PIC,
    cust.VC_ALIAS_NAME SOURCE,
    (SELECT description
    FROM MAMAS.cr_gene_para
    WHERE key1 LIKE '%OWNR%'
    AND VALUE = VC_CAR_OWNERSHIP
    ) POSITION,
    (SELECT --p.app_id_c,
        MAX (k.description) loanpurpose
    FROM MAMAS.cr_gene_para k,
        MACAS.los_app_applications p
    WHERE p.app_id_c = :did
    and k.VALUE = p.LAA_LOAN_PURPOSE_C
    AND k.key1   IN ('LN_PURP', 'LN_PURA', 'LN_PURH')
    GROUP BY p.app_id_c) LOANPURPOSE,
    (SELECT x.statedesc
                FROM MAMAS.nbfc_state_m x, JAVA_LEAMA.nbfc_address_m d
                WHERE x.stateid = d.stateid
                and d.bpid = cust.cust_id_n
                and d.addresstype = 'CURRES')
                CURRENT_ADDRESS  ,
    (SELECT x.statedesc
                FROM MAMAS.nbfc_state_m x,JAVA_LEAMA.nbfc_address_m d
                WHERE x.stateid = d.stateid
                and d.bpid = cust.cust_id_n
                and d.addresstype = 'PERMNENT')
                PER_ADDRESS ,
    (SELECT d.vc_residence_type from JAVA_LEAMA.nbfc_address_m d where d.bpid = cust.cust_id_n and d.addresstype = 'CURRES') VC_RESIDENCE_TYPE,
    (SELECT d.mobile from JAVA_LEAMA.nbfc_address_m d where d.bpid = cust.cust_id_n and d.addresstype = 'CURRES') MOBILE,
    (select phone1
                FROM MACAS.nbfc_reference_m m
                where m.ref_relation in ('WH')
                and app_id_c = :did) PHONE,
    (select ins.INSURANCE_COMPANY from MACAS.LOS_APP_MULTIPLE_INSURANCE ins where app.app_id_c = ins.app_id_c) INSURANCE_COMPANY
FROM MACAS.los_app_applications app,
    MACAS.nbfc_customer_m cust,
 MAMAS.LEA_SCHEME_M sch
    WHERE app.cust_id_n = cust.cust_id_n
 and app.LSM_SCHEMEID = sch.SCHEMEID
    AND app.app_id_c = :did`;

