exports.searchQLDulieu = (req, res, next) => {
    let validation = __utils.validation({
        type: { type: Number },
        id: { type: Number }
    }, req.body)
    // let key = 388347
    // console.log(req.body)
    let queryFinn = require('./queryFinn.js')
        __oracledb.autoCommit = false;
        __oracledb.getConnection(
        {
            user          : __oracleConfigMC.user,
            password      : __oracleConfigMC.password,
            connectString : __oracleConfigMC.connectString
        },
        function (err, connection) {
            if (err) {
                console.log('err ' + err.message)
                return
            }
            connection.execute(queryFinn,
              [req.body.id],
              { maxRows: 5 },
              function (err, result) {
                if (err) {
                    doRelease(connection)
                    return;
                }
                if (result.rows.length == 0) {
                    return __response.dontExits(res, 'Code')
                } else {
                    let resultdata = {}
                    for (let i in result.metaData) {
                        resultdata[result.metaData[i].name] = result.rows[0][i]
                    }
                    resultdata['PushUND'] = true
                    // console.log(resultdata)
                    return __response.success(res, resultdata)
                }
                doRelease(connection)
              })
          });
        function doRelease(connection) {
          connection.close(
            function(err) {
              if (err) {
                console.log('doRelease' + err.message)
              }
            });
        }
}

exports.rejectStage = (req, res, next) => {
    let validation = __utils.validation({
        id: { type: Number }
    }, req.body)

    __oracledb.getConnection(
    {
        user          : __oracleConfigMC.user,
        password      : __oracleConfigMC.password,
        connectString : __oracleConfigMC.connectString
    },
    function (err, connection) {
        if (err) {
            console.log('err ' + err.message)
            return
        }

        var bindvars = {
          v_did: req.body.id
        };
        connection.execute(
          "BEGIN mafc_reject_action(:v_did); END;",
          bindvars,
          function (err, result) {
            if (err) {
              console.error(err);
              // doRelease(connection);
              return;
            }
            __oracledb.getConnection(
            {
                user          : __oracleConfigMC.user,
                password      : __oracleConfigMC.password,
                connectString : __oracleConfigMC.connectString
            },
            function (err, connection) {
                if (err) {
                    console.log('err ' + err.message)
                    return
                }
                connection.execute(
                  "select * from los_app_status where app_id_c = " + req.body.id,
                  function (err, result) {
                    if (err) {
                      console.error(err);
                      return;
                    }
                    for (let i in result.rows) {
                        if (result.rows[i][3] === 'REJ') {
                            return __response.success(res, 'REJ success')
                        }
                    }
                });
            });
            // doRelease(connection);
        });
    });

    // function doRelease(connection) {
    //   connection.close(
    //     function(err) {
    //       if (err) {
    //         console.error(err.message);
    //       }
    //     });
    // }
}

exports.nextStage = (req, res, next) => {
    let validation = __utils.validation({
        id: { type: Number }
    }, req.body)

    __oracledb.getConnection(
    {
        user          : __oracleConfigMC.user,
        password      : __oracleConfigMC.password,
        connectString : __oracleConfigMC.connectString
    },
    function (err, connection) {
        if (err) {
            console.log('err ' + err.message)
            return
        }

        var bindvars = {
          v_did: req.body.id
        };
        connection.execute(
          "BEGIN mafc_next_srr(:v_did); END;",
          bindvars,
          function (err, result) {
            if (err) {
              console.error(err);
              return;
            }
            __oracledb.getConnection(
            {
                user          : __oracleConfigMC.user,
                password      : __oracleConfigMC.password,
                connectString : __oracleConfigMC.connectString
            },
            function (err, connection) {
                if (err) {
                    console.log('err ' + err.message)
                    return
                }
                connection.execute(
                  "select * from los_app_status where app_id_c = " + req.body.id,
                  function (err, result) {
                    if (err) {
                      console.error(err);
                      return;
                    }
                    for (let i in result.rows) {
                        if (result.rows[i][3] === 'SRR') {
                            return __response.success(res, 'SRR success')
                        }
                    }
                });
            });
        });
    });
}

exports.upFiles = (req, res, next) => {
    let formidable = __formidable
    let form = new formidable.IncomingForm()
        form.parse(req, function (err, fields, files) {
          let oldpath = files.cicres.path
          let newpath = __pathUND + files.cicres.name
          __fs.rename(oldpath, newpath, function (err) {
            if (err) throw err
            return __response.success(res, 'upload success')
          })
     })
}

exports.getFiles = (req, res, next) => {
    let validation = __utils.validation({
        type: { type: Number },
        id: { type: Number }
    }, req.body)

    let data = []
    __fs.readdir(__pathUND, function (err, files) {
        if (err) throw err
        for (let index in files) {
            if (files[index].indexOf(req.body.id) >= 0) {
                data.push(files[index]);
            }
        }
        return __response.success(res, data)
    }) 
}
