const writeLogFile = (desc) => {
    __fs.appendFile('./log.txt', '' + new Date() + ' | ' + desc + '\n',  function(err) {
        if (err) {
            return console.error(err)
        }
    })
}

const writeLogFile33 = (desc) => {
    __fs.appendFile('./log33.txt', '' + new Date() + ' | ' + desc + '\n',  function(err) {
        if (err) {
            return console.error(err)
        }
    })
}

const writeLogFileToolAD = (desc) => {
    __fs.appendFile('./logToolAD.txt', '' + new Date() + ' | ' + desc + '\n',  function(err) {
        if (err) {
            return console.error(err)
        }
    })
}

const checkFileExists = (file, callback) => {
    let check = false
    __fs.readdir(__pathFiles, function (err, files) {
        if (err) throw err
        for (let index in files) {
            if (file == files[index]) {
                check = true
            }
        }
        callback(check)
    })
}

const cloneFileToServer = (nameFile, result, datefile, req, res) => {
    var conn = new __client()
    conn.on('ready', function() {
        conn.sftp(function(err, sftp) {
            if (err) throw err
            let fileName = __moment().format('DDMMYY')
            let moveFrom = __pathRpSQL + '' + fileName + '/' + result
            let moveTo = __pathFiles + datefile + ' ' + nameFile + '.csv'
            sftp.fastGet(moveFrom, moveTo , {}, function(downloadError) {
                if(downloadError) throw downloadError
                writeLogFile(nameFile + '| user: ' + req.userSession._id + ', ' + result + ' -> ' + datefile + ' ' + nameFile + '.csv')
                return __response.success(res, result)
            })
        });
    }).connect(__sftpConfigSQL)
}

const cloneFileToServer2 = (nameFile, result, datefile, req, res) => {
    var conn = new __client()
    conn.on('ready', function() {
        conn.sftp(function(err, sftp) {
            if (err) throw err
            let moveFrom = __pathRpORA + '' + result
            let moveTo = __pathFiles + datefile + ' ' + nameFile + '.csv'
            sftp.fastGet(moveFrom, moveTo , {}, function(downloadError) {
                if(downloadError) throw downloadError
                writeLogFile(nameFile + '| user: ' + req.userSession._id + ', ' + result + ' -> ' + datefile + ' ' + nameFile + '.csv')
                return __response.success(res, result)
            })
        });
    }).connect(__sftpConfigORA)
}

const cloneFileToServer3 = (req, res) => {
    var conn = new __client()
    let fullYear = new Date().getFullYear()
    let month = (new Date().getMonth() + 1) >= 10 ? (new Date().getMonth() + 1) : '0' + (new Date().getMonth() + 1)
    let date = new Date().getDate() >= 10 ? new Date().getDate() : '0' + new Date().getDate()
    let filename = 'MAFC_CICSCORE_REQ_PL_' + fullYear + '_' + month + '_' + date
    let listfile = []
    conn.on('ready', function() {
        conn.sftp(function(err, sftp) {
            sftp.readdir(__pathRpCIC + 'OUTBOX/', function(err, list) {
                list.forEach((item, i) => {
                    if (item.filename.indexOf(filename) >= 0) {
                        listfile.push(item.filename)
                    }
                })
                listfile.forEach((item, i) => {
                    let moveFrom = __pathRpCIC + 'OUTBOX/' + item
                    let moveTo = __pathFiles + item
                    sftp.fastGet(moveFrom, moveTo , {}, function(downloadError) {
                        if(downloadError) throw downloadError
                        writeLogFile('CICREQ' + '| user: ' + req.userSession._id + ', ' + item)
                        if (listfile.length == i + 1) {
                            return __response.success(res, 'success')
                        }
                    })
                })
            });
        });
    }).connect(__sftpConfigORA)
}

exports.searchTSL = (req, res, next) => {
    let validation = __utils.validation({
        custIdNo: { type: Number },
        mobile: { type: Number },
        custName: { type: String }
    }, req.body)

    let connection = new __connection(__sqlserverCofig)
    connection.on('connect', function(err) {
        executeStatement()
    })
    const executeStatement = () => {
        request = new __requestsql(`select * from telesales_leads WHERE id_no = '` 
            + req.body.custIdNo 
            + `' or Mobile = '` 
            + req.body.mobile 
            + `' or FullName like '%`
            + req.body.custName + `%';`,
        function(err) {  
        if (err) {  
            console.log(err)}  
        });  
        let result = {}
        request.on('row', function(columns) {
            columns.forEach(function(column) {
              if (column.value === null) {
              } else {
                result[column.metadata.colName] = column.value
              }  
            })
            return __response.success(res, result)
        })

        request.on('requestCompleted', function (rowCount, more) {
            if (Object.keys(result).length === 0 && result.constructor === Object) {
                return __response.dontExits(res, 'Cust')
            }
        })

        connection.execSql(request)
    }
}

exports.deleteTSL = (req, res, next) => {
    let validation = __utils.validation({
        custIdNo: { type: Number },
        mobile: { type: Number },
        custName: { type: String }
    }, req.body)

    let connection = new __connection(__sqlserverCofig)
    connection.on('connect', function(err) {
        executeStatement()
    })

    const executeStatement = () => {  
        request = new __requestsql(`insert into telesales_leads_bk_cc select * from telesales_leads WHERE id_no = '` 
            + req.body.custIdNo 
            + `' or Mobile = '` 
            + req.body.mobile 
            + `' or FullName like '%`
            + req.body.custName + `%';`,
        function(err) {  
        if (err) {  
            console.log(err)}  
        })
        request.on('requestCompleted', function (rowCount, more) {
            executeStatementde(req.body.custIdNo, req.body.mobile, req.body.custName)
        })
        connection.execSql(request)
    }
    const executeStatementde = (a, b, c) => {  
        request = new __requestsql(`delete telesales_leads WHERE id_no = '` 
            + a 
            + `' or Mobile = '` 
            + b 
            + `' or FullName like '%`
            + c + `%';`,
        function(err) {  
        if (err) {  
            console.log(err)}  
        })
        request.on('requestCompleted', function (rowCount, more) {
            writeLogFile33('TLS33:  ' + a + ' mobile: ' + b)
            writeLogFileToolAD('TLS33:  ' + a + ' mobile: ' + b || 'null')
            return __response.success(res, 'ok')
        })
        connection.execSql(request)
    }
}

exports.searchTeDrs = (req, res, next) => {
    let validation = __utils.validation({
        code: { type: String }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    __oracledb.autoCommit = false;
    __oracledb.getConnection(
    {
        user          : __oracleConfigMM.user,
        password      : __oracleConfigMM.password,
        connectString : __oracleConfigMM.connectString
    },
    function (err, connection) {
        if (err) {
            console.log('err ' + err.message)
            return
        }
        connection.execute(
          `SELECT inspectorid, inspectorname, region, status, makedate, authdate
           FROM mamas.lea_inspector_m
           WHERE inspectorname = :did`,
          [req.body.code],
          { maxRows: 5 },
          function (err, result) {
            if (err) {
                doRelease(connection)
                return;
            }
            if (result.rows.length == 0) {
                return __response.dontExits(res, 'Code')
            } else {
                return __response.success(res, result)
            }
            doRelease(connection)
          })
      });
    function doRelease(connection) {
      connection.close(
        function(err) {
          if (err) {
            console.log('doRelease' + err.message)
          }
        });
    }
}

exports.executetedrs = (req, res, next) => {
    let validation = __utils.validation({
        code: { type: String }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    __oracledb.getConnection(
    {
        user          : __oracleConfigMM.user,
        password      : __oracleConfigMM.password,
        connectString : __oracleConfigMM.connectString
    },
    function (err, connection) {
        if (err) {
            console.log('err ' + err.message)
            return
        }
        connection.execute(
            `SELECT max(inspectorid) FROM mamas.lea_inspector_m`,
            function(err, result) {
            if (err) {
                doRelease(connection)
                return;
            }
            console.log(typeof result.rows[0][0])
            if (typeof result.rows[0][0] == 'number') {
                connection.execute(
                    `INSERT INTO mamas.lea_inspector_m
                    (INSPECTORID,INSPECTORNAME,INCENTIVE,REGION,STATUS,MAKERID,MAKEDATE,AUTHID,AUTHDATE,ROLE,USERID,BROKERID,EXPOSURE_AMT,EXPOSED_AMT,MO_DME_FLAG,ACC_NUMBER,AGENCY,SUPERVISOR,ICBS_AO_CODE,DML_STATUS,MC_STATUS,BANK_NAME_C,BRANCH_NAME_C,PAYMENT_MODE_C,BANK_ACCNO_C)
                     VALUES 
                    (:0, :1,0,1,'A','SYSTEM',sysdate,'SYSTEM',sysdate,'S',null,2,null,null,'DM',null,null,null,null,'S','A',null,null,null,null)`,
                    [ result.rows[0][0] + 1, req.body.code],
                    { autoCommit: true },
                    function(err, result) {
                    if (err) {
                        doRelease(connection)
                        return __response.dontExits(res, result)
                    }
                    writeLogFileToolAD('Add TE/DRS:  ID: ' + (result.rows[0][0] + 1) + ' name: ' + req.body.code)
                    return __response.success(res, result)
                    doRelease(connection)
                })
            }
          })
      });
    function doRelease(connection) {
      connection.close(
        function(err) {
          if (err) {
            console.log('doRelease' + err.message)
          }
        });
    }
}

exports.searchInforCust = (req, res, next) => {
    let validation = __utils.validation({
        app_id: { type: Number }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }
    let results = {}
    results.appId = req.body.app_id
    let connection = new __connection(__sqlserverCofig)
    connection.on('connect', function(err) {
        executeStatement()
    })

    const executeStatement = () => {
        request = new __requestsql(`select distinct v.LAA_SOURCE_INSPECTORID_N, w.INSPECTORID, w.INSPECTORNAME
                                     from dbo.LOS_APP_APPLICATIONS v, AGENT_CODE w
                                     where v.LAA_SOURCE_INSPECTORID_N = w.INSPECTORID
                                     and v.APP_ID_C = ` + req.body.app_id,
        function(err) {  
        if (err) {  
            console.log(err)}  
        })
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.metadata.colName == 'INSPECTORID') {
                    results.inspectorIdOld = column.value
                }
                if (column.metadata.colName == 'INSPECTORNAME') {
                    results.inspectorNameOld = column.value
                }
            })
        })

        request.on('requestCompleted', function (rowCount, more) {
            if (results.inspectorIdOld == undefined) {
                return __response.dontExits(res, 'App')
            } else {
                return __response.success(res, results)
            }
        })

        connection.execSql(request)
    }
}

exports.searchInforDSAnew = (req, res, next) => {
    let validation = __utils.validation({
        newDSA: { type: String }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }
    let results = {}
    let connection = new __connection(__sqlserverCofig)
    connection.on('connect', function(err) {
        executeStatementNewDSA()
    })

    const executeStatementNewDSA = () => {
        request = new __requestsql(`select INSPECTORID, INSPECTORNAME from dbo.AGENT_CODE WHERE INSPECTORNAME = '` + req.body.newDSA + `';`,
        function(err) {  
        if (err) {  
            console.log(err)}  
        })
        let result = null
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.metadata.colName == 'INSPECTORID') {
                    results.inspectorIdNew = column.value
                }
                if (column.metadata.colName == 'INSPECTORNAME') {
                    results.inspectorNameNew = column.value
                }
            })
        })

        request.on('requestCompleted', function (rowCount, more) {
            if (__utils.isObject(results)) {
                return __response.success(res, results)
            } else {
                return __response.dontExits(res, 'DSA')
            }
        })

        connection.execSql(request)
    }
}

exports.executeInforDSAnew = (req, res, next) => {
    let validation = __utils.validation({
        app_id: { type: Number },
        oldDSA: { type: Number },
        newDSA: { type: Number }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let connection = new __connection(__sqlserverCofig)
    connection.on('connect', function(err) {
        executeStatementNewDSA()
    })

    const executeStatementNewDSA = () => {
        request = new __requestsql(`update dbo.LOS_APP_APPLICATIONS set LAA_SOURCE_INSPECTORID_N = ` + req.body.newDSA + ` WHERE APP_ID_C = ` + req.body.app_id + `;`,
        function(err) {  
        if (err) {  
            console.log(err)}  
        })
        request.on('row', function(columns) {
        })

        request.on('requestCompleted', function (rowCount, more) {
            writeLogFileToolAD('Update_DSA:  App_ID: ' + req.body.app_id + ' Inspector_ID_Old: ' + req.body.oldDSA + ' --> Inspector_ID_New: ' + req.body.newDSA)
            executeStatement(req.body.app_id)
        })

        connection.execSql(request)
    }

    const executeStatement = (value) => {
        let results = {}
        results.appId = value
        request = new __requestsql(`select distinct v.LAA_SOURCE_INSPECTORID_N, w.INSPECTORID, w.INSPECTORNAME
                                     from dbo.LOS_APP_APPLICATIONS v, AGENT_CODE w
                                     where v.LAA_SOURCE_INSPECTORID_N = w.INSPECTORID
                                     and v.APP_ID_C = ` + value,
        function(err) {  
        if (err) {  
            console.log(err)}  
        })
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.metadata.colName == 'INSPECTORID') {
                    results.inspectorIdOld = column.value
                }
                if (column.metadata.colName == 'INSPECTORNAME') {
                    results.inspectorNameOld = column.value
                }
            })
        })

        request.on('requestCompleted', function (rowCount, more) {
            if (results.inspectorIdOld == undefined) {
                return __response.dontExits(res, 'App')
            } else {
                return __response.success(res, results)
            }
        })

        connection.execSql(request)
    }
}

exports.epCollNote = (req, res, next) => {
    let validation = __utils.validation({
        date: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    checkFileExists(req.body.date + ' COLL_NOTE.csv', (check) => {
        if (check) {
            return __response.success(res, req.body.date + ' COLL_NOTE.csv exists')
        } else {
            let connection = new __connection(__sqlserverCofig)
            connection.on('connect', function(err) {
                executeStatement()
            })

            const executeStatement = () => {
                request = new __requestsql( 
                    `declare @flag int,@filename nvarchar(200)
                    exec [sp_get_coll_note_report] '` 
                    + req.body.date
                    + `', @flag out, @filename out
                    select @flag as flag, @filename as filename`,
                function(err) {  
                    if (err) {  
                        console.log(err)
                    }  
                })
                let result = String
                request.on('row', function(columns) {
                    columns.forEach(function(column) {
                        if (column.metadata.colName === 'filename') {
                            result = column.value
                            cloneFileToServer('COLL_NOTE', result, req.body.date, req, res)
                        }
                    })
                })
                connection.execSql(request)
            }
        }
    })
}

exports.epDRSDaily = (req, res, next) => {
    let toDate = __moment().format('YYYYMMDD')
    checkFileExists(toDate + ' DRS_DAILY.csv', (check) => {
        if (check) {
            return __response.success(res, toDate + ' DRS_DAILY.csv exists')
        } else {
            let connection = new __connection(__sqlserverCofig)
            connection.on('connect', function(err) {
                executeStatement()
            })

            const executeStatement = () => {
                request = new __requestsql(
                    `declare @flag int, @filename nvarchar(500)
                    exec [sp_get_DRS_DAILY_RP_report] @filename out, @flag out
                    select @flag as flag ,@filename as filename`,
                function(err) {  
                    if (err) {  
                        console.log(err)
                    }  
                })
                let result = String
                request.on('row', function(columns) {
                    columns.forEach(function(column) {
                        if (column.metadata.colName === 'filename') {
                            result = column.value
                            cloneFileToServer('DRS_DAILY', result, toDate, req, res)
                        }
                    })
                })
                connection.execSql(request)
            }
        }
    })
}

exports.epUndRecord = (req, res, next) => {
    let validation = __utils.validation({
        date: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    checkFileExists(req.body.date + ' UND_RECORD.csv', (check) => {
        if (check) {
            return __response.success(res, req.body.date + ' UND_RECORD.csv exists')
        } else {
            let connection = new __connection(__sqlserverCofig)
            connection.on('connect', function(err) {
                executeStatement()
            })

            const executeStatement = () => {
                request = new __requestsql( 
                    `declare @flag int,@filename nvarchar(200)
                    exec [sp_get_und_record_report] '` 
                    + req.body.date
                    + `', @flag out, @filename out
                    select @flag as flag, @filename as filename`,
                function(err) {  
                    if (err) {  
                        console.log(err)
                    }  
                })
                let result = String
                request.on('row', function(columns) {
                    columns.forEach(function(column) {
                        if (column.metadata.colName === 'filename') {
                            result = column.value
                            cloneFileToServer('UND_RECORD', result, req.body.date, req, res)
                        }
                    })
                })
                connection.execSql(request)
            }
        }
    })
}

exports.penRecord = (req, res, next) => {
    let validation = __utils.validation({
        dateFr: { type: String, required: true },
        dateTo: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }
    let fileName = req.body.dateFr + '-' + req.body.dateTo + ' PENDING_RECORD.csv'
    checkFileExists(fileName, (check) => {
        if (check) {
            return __response.success(res, fileName + ' exists')
        } else {
            
        }
    })
}

exports.cicREQ = (req, res, next) => {
    __oracledb.autoCommit = false;
console.log('1')
    __oracledb.getConnection(
    {
        user          : __oracleConfigMC.user,
        password      : __oracleConfigMC.password,
        connectString : __oracleConfigMC.connectString
    },
    function (err, connection) {
        if (err) {
            console.log('err ' + err.message)
            return
        }
        connection.execute(
          "BEGIN SP_CIC_REQUEST_MAIN; END;",
          function (err, result) {
            if (err) {
                doRelease(connection)
                return __response.err(err, next)
            }
console.log('2')
	    writeLogFile('CICREQ' + '| user: ' + req.userSession._id)
            cloneFileToServer3(req, res)
            doRelease(connection)
          });
      });

    function doRelease(connection) {
      connection.close(
        function(err) {
          if (err) {
            console.log('doRelease' + err.message)
          }
        });
    }
}

exports.cicRES = (req, res, next) => {
    let validation = __utils.validation({
        filename: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    var conn = new __client()
    conn.on('ready', function() {
        conn.sftp(function(err, sftp) {
            let moveTo = __pathRpCIC + 'INBOX/' + req.body.filename
            let moveFrom = __pathFiles + req.body.filename
            sftp.fastPut(moveFrom, moveTo , {}, function(downloadError) {
                if(downloadError) throw downloadError
                writeLogFile('CICRES' + '| user: ' + req.userSession._id + ', ' + req.body.filename)
                // __oracledb.autoCommit = false;
                __oracledb.getConnection(
                {
                    user          : __oracleConfigMC.user,
                    password      : __oracleConfigMC.password,
                    connectString : __oracleConfigMC.connectString
                },
                function (err, connection) {
                    if (err) {
                        console.log('err ' + err.message)
                        return
                    }
                    connection.execute(
                      "BEGIN SP_CIC_RESPONSE_MAIN; END;",
                      function (err, result) {
                        if (err) {
                            doRelease(connection)
                            return __response.err(err, next)
                        }
                        return __response.success(res, 'success')
                        doRelease(connection)
                      });
                  });

                function doRelease(connection) {
                  connection.close(
                    function(err) {
                      if (err) {
                        console.log('doRelease' + err.message)
                      }
                    });
                }
            })
        });
    }).connect(__sftpConfigORA)


    console.log(req.body.filename)
}

exports.collDuedate = (req, res, next) => {
    let validation = __utils.validation({
        date: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    checkFileExists(req.body.date + ' DUE_DATE.csv', (check) => {
        if (check) {
            return __response.success(res, req.body.date + ' DUE_DATE.csv exists')
        } else {
            __oracledb.autoCommit = false;
            __oracledb.getConnection(
            {
                user          : __oracleConfig.user,
                password      : __oracleConfig.password,
                connectString : __oracleConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.log('err ' + err.message)
                    return
                }
                var bindvars = {
                    v_duedate: req.body.date,
                    v_expfile: { dir: __oracledb.BIND_OUT, type: __oracledb.STRING }
                };
                connection.execute(
                  "BEGIN SQLSERVER.EF_COLL_DUEDATE(:v_duedate, :v_expfile); END;",
                  bindvars,
                  function (err, result) {
                    if (err) {
                        doRelease(connection)
                        return;
                    }
                    cloneFileToServer2('DUE_DATE', result.outBinds.v_expfile, req.body.date, req, res)
                    doRelease(connection)
                  });
              });

            function doRelease(connection) {
              connection.close(
                function(err) {
                  if (err) {
                    console.log('doRelease' + err.message)
                  }
                });
            }
        }
    })
}

exports.dpdDaily = (req, res, next) => {
    let toDate = __moment().format('YYYYMMDD')
    checkFileExists(toDate + ' DPD_DAILY.csv', (check) => {
        if (check) {
            return __response.success(res, toDate + ' DPD_DAILY.csv exists')
        } else {
            __oracledb.autoCommit = false;
            __oracledb.getConnection(
                {
                    user          : __oracleConfig.user,
                    password      : __oracleConfig.password,
                    connectString : __oracleConfig.connectString
                },
                function (err, connection) {
                    if (err) {
                        console.log('err ' + err.message)
                        return
                    }
                    var bindvars = {
                        v_expfile: { dir: __oracledb.BIND_OUT, type: __oracledb.STRING }
                    };
                    connection.execute(
                        "BEGIN SQLSERVER.EF_COLL_DPD(:v_expfile); END;",
                        bindvars,
                        function (err, result) {
                            if (err) {
                                doRelease(connection)
                                return;
                            }
                            cloneFileToServer2('DPD_DAILY', result.outBinds.v_expfile, toDate, req, res)
                            doRelease(connection)
                        }
                    );
                }
            );

            function doRelease(connection) {
              connection.close(
                function(err) {
                  if (err) {
                    console.log('doRelease' + err.message)
                  }
                });
            }
        }
    })
}


exports.smsDaily = (req, res, next) => {
    let validation = __utils.validation({
        dateFr: { type: String, required: true },
        dateTo: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }
    let fileName = req.body.dateFr + '-' + req.body.dateTo + ' SMS_RECORD.csv'
    checkFileExists(fileName, (check) => {
        if (check) {
            return __response.success(res, fileName + ' exists')
        } else {
            __oracledb.autoCommit = false;
            __oracledb.getConnection(
            {
                user          : __oracleConfig.user,
                password      : __oracleConfig.password,
                connectString : __oracleConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.log('err ' + err.message)
                    return
                }
                var bindvars = {
                    v_duedatefr: req.body.dateFr,
                    v_duedateto: req.body.dateTo,
                    v_expfile: { dir: __oracledb.BIND_OUT, type: __oracledb.STRING }
                };
                connection.execute(
                  "BEGIN SQLSERVER.EF_COLL_SMS(:v_duedatefr, :v_duedateto, :v_expfile); END;",
                  bindvars,
                  function (err, result) {
                    if (err) {
                        doRelease(connection)
                        return;
                    }
                    cloneFileToServer2('SMS_RECORD', result.outBinds.v_expfile, req.body.dateFr + '-' + req.body.dateTo, req, res)
                    doRelease(connection)
                  });
              });

            function doRelease(connection) {
              connection.close(
                function(err) {
                  if (err) {
                    console.log('doRelease' + err.message)
                  }
                });
            }
        }
    })
}

exports.getUserFiles = (req, res, next) => {
    let roles = req.userSession.roleFiles
    __fs.readdir(__pathFiles, function (err, files) {
        if (err) throw err
        var filenames = []
        for (var index in files) {
            if (req.userSession._id == process.env.USER_ADMIN) {
                filenames.push(files[index])
            }
            if (roles != undefined) {
                roles.forEach((role) => {
                    if (files[index].indexOf(role) > 0) {
                        filenames.push(files[index])
                    }
                })
            }
        }
        return __response.success(res, filenames)
    })
}

exports.downFiles = (req, res, next) => {
    let validation = __utils.validation({
        desc: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let stat = __fs.statSync(__pathFiles + req.body.desc)

    res.writeHead(200, {
        'Content-Type': 'text/*',
        'Content-Length': stat.size
    })

    let readStream = __fs.createReadStream(__pathFiles + req.body.desc)

    readStream.pipe(res)
}

exports.upFiles = (req, res, next) => {
    let formidable = __formidable
    let form = new formidable.IncomingForm()
        form.parse(req, function (err, fields, files) {
          let oldpath = files.cicres.path
          let newpath = __pathFiles + files.cicres.name
          __fs.rename(oldpath, newpath, function (err) {
            if (err) throw err
            return __response.success(res, 'upload success')
          })
     })
}

// exports.executeScript = (req, res, next) => {
//     let validation = __utils.validation({
//         script: { type: String, required: true }
//     }, req.body)

//     if (__utils.isObject(validation.fail)) {
//         return __response.invalidData(res, validation.fail)
//     }

//     let connection = new __connection(__sqlserverCofig)
//     connection.on('connect', function(err) {
//         executeStatement()
//     })
//     const executeStatement = () => {
//         request = new __requestsql( req.body.script,
//         function(err) {  
//         if (err) {  
//             console.log(err)}  
//         });  
//         let result = {}
//         request.on('row', function(columns) {
//             // console.log(columns)
//             // columns.forEach(function(column) {
//             //     // console.log(column)
//             //   if (column.value === null) {
//             //   } else {
//             //     // result[column.metadata.colName] = column.value
//             //   }  
//             // })
//             // return __response.success(res, result)
//         })

//         request.on('requestCompleted', function (rowCount, more) {
//             console.log(rowCount)
//             // if (Object.keys(result).length === 0 && result.constructor === Object) {
//             //     return __response.dontExits(res, 'Cust')
//             // }
//         })

//         connection.execSql(request)
//     }
// }
