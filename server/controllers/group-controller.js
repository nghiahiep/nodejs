exports.create = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        name: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, creator: req.userSession._id, editor: req.userSession._id, createdAt: __utils.dateNow(), updatedAt: __utils.dateNow() }

    __group.create(object)
        .then(docs => {
            __group.updateMulti({ $or: [{ _id: object.groupOwner }, { groupChildrens: object.groupOwner }] }, { $push: { groupChildrens: docs._id } }).then()
            return __response.create(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}

exports.reads = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: { $nin: [process.env.GROUP_ADMIN, req.userSession._id] } })
    __group.reads(req._query)
    //__group.reads(req._query, req._skip, req._limit, req._sort)
        .populate('editor', 'firstName lastName email')
        .populate('groupOwner', 'name')
        .then(list => {
            return __group.count(req._query)
                .then(total => {
                    return __response.reads(res, list, req._skip, req._limit, total)
                })
        })
        .catch(err => {
            return next(err)
        })
}

exports.read = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: req.params._id })

    __group.read(req._query)
        .populate('editor', 'firstName lastName email')
        .populate('groupOwner', 'name')
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'Group')
            }
            return __response.success(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}

exports.update = (req, res, next) => {
    let validation = __utils.validation({
        groupOwner: { type: String, required: true },
        name: { type: String, required: true }
    }, req.body)

    if (__utils.isObject(validation.fail)) {
        return __response.invalidData(res, validation.fail)
    }

    let object = { ...validation.ok, editor: req.userSession._id, updatedAt: __utils.dateNow() }

    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: { $nin: [process.env.GROUP_ADMIN, req.userSession._id] } })
    req._query['$and'].push({ _id: req.params._id })

    __group.update(req._query, object)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'Group')
            }

            if (object.groupOwner != docs.groupOwner) {
                let groups = [docs._id].concat(docs.groupChildrens)
                __group.updateMulti({ $or: [{ _id: docs.groupOwner }, { groupChildrens: docs._id }] }, { $pull: { groupChildrens: { $in: groups } } })
                    .then(pull => {
                        __group.updateMulti({ $or: [{ _id: object.groupOwner }, { groupChildrens: object.groupOwner }] }, { $push: { groupChildrens: { $each: groups } } }).then()
                    })
            }
            return __response.success(res, Object.assign(docs, object))
        })
        .catch(err => {
            return next(err)
        })
}

exports.delete = (req, res, next) => {
    req._query['$and'].push({ groupOwner: { $in: req.userSession.groupChildrens } })
    req._query['$and'].push({ _id: { $nin: [process.env.GROUP_ADMIN , req.userSession._id] } })
    req._query['$and'].push({ _id: req.params._id })

    __group.delete(req._query)
        .then(docs => {
            if (!docs) {
                return __response.dontExits(res, 'Group')
            }
            __group.updateMulti({ $or: [{ _id: docs.groupOwner }, { groupChildrens: docs._id }] }, { $pull: { groupChildrens: docs._id } }).then()
            __user.reads({ groupOwner: docs._id }, null, null, null, 'token')
                .then(users => {
                    for (let i in users) {
                        __redis.del('token_' + users[i].token)
                    }
                })

            return __response.success(res, docs)
        })
        .catch(err => {
            return next(err)
        })
}

exports.getForSelect2 = (req, res, next) => {
    req._query['$and'].push({ $or: [{ groupOwner: { $in: req.userSession.groupChildrens } }, { _id: { $in: [req.userSession.groupOwner, req.userSession.groupDefault] } }] })

    __group.reads(req._query, null, null, { name: 'asc' }, 'name')
        .then(list => {
            list = __utils.copy(list)
            list.forEach(item => {
                item.id = item._id
                item.text = item.name
            })
            return __response.success(res, list)
        })
        .catch(err => {
            return next(err)
        })
}