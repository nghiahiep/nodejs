exports.refresh = () => {
    //__redis.del('extRoom_PREDUE_TEAM_LEAD')
    //__redis.del('extRoom_SUPERVISOR')
    //__redis.del('extRoom_PREDUE_TEAM')
    //__redis.del('extRoom_B2_TEAM')
    //__redis.del('extRoom_B1_NEW_TEAM')
    //__redis.del('extRoom_B1_BOM_TEAM')
    let query = { '$and': [ {} ] }
    __ext.reads(query)
        .populate('groupOwner', 'name')
        .populate('user', 'user')
        .then(list => {
            let arrTempt = []
            list.forEach(item => {
                let pushNewExt = true
                let obj = (Object.assign(item.toObject(), { status: 'Offline' }, { phone: null }, { timeUpdated: __utils.dateNowTimeSpan() }))
                if (item.groupOwner != null) {
                    for (let i in arrTempt) {
                        if (arrTempt[i].gr === item.groupOwner.name ) {
                            pushNewExt = false
                            arrTempt[i].ext.push(obj)
                        }
                    }
                    if (pushNewExt) {
                        arrTempt.push({ gr: item.groupOwner.name, ext: [obj] })
                    }
                }
                // if (item.groupOwner != null) {
                //     let nameRoom = 'extRoom_' + item.groupOwner.name
                //     let pushNewExt = true
                //     let extRoom = []
                //     // __redis.del(nameRoom)
                //     // let extUpdated = Object.assign(data.ext, data.status, { timeUpdated: __utils.dateNowTimeSpan() })
                //     __redis.get(nameRoom, (err, roomsRedis) => {
                //         if (err || roomsRedis == null) {
                //             extRoom = [item]
                //              __redis.set(nameRoom, extRoom)
                //         } else {
                //             for (let i = 0; i < roomsRedis.length; i++) {
                //                 if(roomsRedis[i].ext === item.ext) {
                //                     pushNewExt = false
                //                     roomsRedis[i] = Object.assign(item, { status: roomsRedis[i].status }, { phone: roomsRedis[i].phone })
                //                     __redis.set(nameRoom, roomsRedis)
                //                     // ext[i] = Objecư.assign(data.ext, data.status, { timeUpdated: __utils.dateNowTimeSpan() })
                //                 }
                //             }
                //             if (pushNewExt) {
                //                 roomsRedis.push(item)
                //                 __redis.set(nameRoom, roomsRedis)
                //                 // roomsRedis.push(Object.assign(item, { status: roomsRedis[i].status }, { phone: roomsRedis[i].phone }))
                //             }
                //             // extRoom = roomsRedis
                //         }
                //         // let roomData = {
                //         //     group: data.ext.groupOwner.name,
                //         //     timeSystem: __utils.dateNowTimeSpan(),
                //         //     users: [extUpdated]
                //         // }
                //         // socketNameSpace.in(nameRoom).emit('broadcast', roomData)
                //     })
                // }
            })
            for (let i in arrTempt) {
                let nameRoom = 'extRoom_' + arrTempt[i].gr
                console.log(nameRoom)
                __redis.del(nameRoom)
                __redis.set(nameRoom, arrTempt[i].ext)
            }
        })
        .catch(err => {
            console.log(err)
        })
}

exports.update = (status, ext) => {
    if (ext.groupOwner != null) {
        let nameRoom = 'extRoom_' + ext.groupOwner.name
        __redis.get(nameRoom, (err, roomsRedis) => {
            if (err || roomsRedis == null) {
                __redis.set(nameRoom, Object.assign(ext.toObject(), { status: 'Offline' }, { phone: null }, { timeUpdated: __utils.dateNowTimeSpan() }))
            } else {
                for (let i in roomsRedis) {
                    if(roomsRedis[i].ext === ext.ext) {
                        if (status === 'delete') {
                            roomsRedis.splice(i, 1)
                        } else {
                            roomsRedis[i] = Object.assign(ext.toObject(), { status: roomsRedis[i].status }, { phone: roomsRedis[i].phone }, { timeUpdated: __utils.dateNowTimeSpan() })
                        }
                    }
                }
                __redis.set(nameRoom, roomsRedis)
            }
        })
    }
}

exports.updateStatus = (data) => {
    let ext = data.ext
    let status = data.status
    let phone = data.phone
    let nameRoom = 'extRoom_' + ext.groupOwner.name
    __redis.get(nameRoom, (err, roomsRedis) => {
        for (let i in roomsRedis) {
            if(roomsRedis[i].ext === ext.ext) {
                roomsRedis[i] = Object.assign(ext, { status: status }, { phone: phone })
                __redis.set(nameRoom, roomsRedis)
            }
        }
    })
}