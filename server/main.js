require('events').EventEmitter.prototype._maxListeners = 0
// if (process.env.NODE_ENV === 'dev') {
    require('dotenv').config({ path: __dirname + '/environments/development.env' })
// }
// else {
//     require('dotenv').config({ path: __dirname + '/environments/production.env' })
// }

__cluster = require('cluster')
if (__cluster.isMaster) {
    let cpuCount = parseInt(process.env.CPU_NUMBER)
    for (let i = 1; i <= cpuCount; i++) {
        __cluster.fork()
        __cluster.on('exit', (code, signal) => {
            console.log(`Worker ${i} died`)
        })
    }
} else {
    require('./server')
}